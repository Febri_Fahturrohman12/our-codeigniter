-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 18 Mei 2018 pada 13.01
-- Versi Server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lowonganpekerjaan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(10) NOT NULL AUTO_INCREMENT,
  `admin_nama` varchar(30) NOT NULL,
  `admin_nopon` int(20) NOT NULL,
  `admin_email` varchar(30) NOT NULL,
  `admin_username` varchar(30) NOT NULL,
  `admin_password` varchar(30) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_nama`, `admin_nopon`, `admin_email`, `admin_username`, `admin_password`) VALUES
(1, 'admin', 12345, 'admin@gmail.com', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambar`
--

CREATE TABLE IF NOT EXISTS `gambar` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `deskripsi` varchar(100) NOT NULL,
  `nama_file` varchar(100) NOT NULL,
  `ukuran_file` varchar(100) NOT NULL,
  `tipe_file` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `gambar`
--

INSERT INTO `gambar` (`id`, `deskripsi`, `nama_file`, `ukuran_file`, `tipe_file`) VALUES
(1, 'abc', 'AngryBirds_Classic_icon.jpg', '37.01', 'image/jpeg'),
(2, 'abc', 'AngryBirds_Classic_icon1.jpg', '37.01', 'image/jpeg'),
(3, 'sapi', 'AngryBirds_Classic_icon2.jpg', '37.01', 'image/jpeg'),
(4, 'sapi', 'Pos-Indonesia-20131.jpg', '235.71', 'image/jpeg'),
(5, 'kkkk', 'AngryBirds_Classic_icon3.jpg', '37.01', 'image/jpeg'),
(6, 'sapisapi', 'person-flat.png', '26.63', 'image/png'),
(7, 'sass', 'person-flat1.png', '26.63', 'image/png'),
(8, 'sdd', 'Pos-Indonesia-201311.jpg', '235.71', 'image/jpeg'),
(9, 'ssssssss', 'Capture1.PNG', '163.01', 'image/png'),
(10, 'hjhjhj', 'Capture11.PNG', '15.84', 'image/png'),
(11, 'ksjdskdj', 'loker.jpg', '648.49', 'image/jpeg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `melamar`
--

CREATE TABLE IF NOT EXISTS `melamar` (
  `melamar_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `pekerjaan_id` int(10) NOT NULL,
  `perusahaan_id` int(10) NOT NULL,
  `user_nama` varchar(225) NOT NULL,
  `user_cv` varchar(225) NOT NULL,
  `pekerjaan_nama` varchar(30) NOT NULL,
  `perusahaan_nama` varchar(30) NOT NULL,
  `user_email` varchar(30) NOT NULL,
  `user_nopon` int(20) NOT NULL,
  `perusahaan_nopon` int(20) NOT NULL,
  PRIMARY KEY (`melamar_id`),
  KEY `pekerjaan_id` (`pekerjaan_id`),
  KEY `perusahaan_id` (`perusahaan_id`),
  KEY `user_id` (`user_id`),
  KEY `pekerjaan_id_2` (`pekerjaan_id`),
  KEY `perusahaan_id_2` (`perusahaan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data untuk tabel `melamar`
--

INSERT INTO `melamar` (`melamar_id`, `user_id`, `pekerjaan_id`, `perusahaan_id`, `user_nama`, `user_cv`, `pekerjaan_nama`, `perusahaan_nama`, `user_email`, `user_nopon`, `perusahaan_nopon`) VALUES
(15, 22, 33, 12, 'Febri Fahturrohman', '017_VisualPort_Jenius_Graphic_Design_Partnership_23Apr-22May_Invoice_010520189.pdf', 'Web Development', 'Kodeku.Net', 'febri.faturrohman10@gmail.com', 2147483647, 2147483647),
(16, 22, 34, 12, 'John Cena', '017_VisualPort_Jenius_Graphic_Design_Partnership_23Apr-22May_Invoice_0105201810.pdf', 'Security Kantor', 'Kodeku.Net', 'Johncena@yahoo.co.id', 2147483647, 23424332);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pekerjaan`
--

CREATE TABLE IF NOT EXISTS `pekerjaan` (
  `pekerjaan_id` int(10) NOT NULL AUTO_INCREMENT,
  `perusahaan_id` int(10) NOT NULL,
  `pekerjaan_nama` varchar(30) NOT NULL,
  `perusahaan_nama` varchar(30) NOT NULL,
  `perusahaan_alamat` text NOT NULL,
  `perusahaan_nopon` int(20) NOT NULL,
  `tipe_gaji` varchar(30) NOT NULL,
  `umur` int(20) NOT NULL,
  `pendidikan` varchar(30) NOT NULL,
  `jenis_kelamin` enum('Laki','Perempuan') NOT NULL,
  `tentang_pekerjaan` text NOT NULL,
  PRIMARY KEY (`pekerjaan_id`),
  KEY `perusahaan_id` (`perusahaan_id`),
  KEY `perusahaan_id_2` (`perusahaan_id`),
  KEY `perusahaan_id_3` (`perusahaan_id`),
  KEY `perusahaan_id_4` (`perusahaan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data untuk tabel `pekerjaan`
--

INSERT INTO `pekerjaan` (`pekerjaan_id`, `perusahaan_id`, `pekerjaan_nama`, `perusahaan_nama`, `perusahaan_alamat`, `perusahaan_nopon`, `tipe_gaji`, `umur`, `pendidikan`, `jenis_kelamin`, `tentang_pekerjaan`) VALUES
(33, 12, 'Web Development', 'Kodeku.Net', '                                                              Sawojajar Malang                                                ', 2147483647, 'Bulanan', 24, 'SMA / SMK', 'Laki', 'Menjadi Web Development                                                       '),
(34, 12, 'Security Kantor', 'Kodeku.Net', 'Batu', 23424332, 'Bulanan', 35, 'S2 ', 'Laki', 'Menjaga Kantor Cabang Batu'),
(35, 12, 'Back End Developer', 'Kodeku.Net', 'Sawojajar Malang', 812321123, 'Bulanan', 20, 'Sederajat', 'Laki', 'Menjadi Developer Back End\r\n\r\n'),
(36, 14, 'Kepala Bidang Prakerin', 'SMK PGRI 3 Malang', 'Tlogomas', 812311231, 'Bulanan', 20, 'SMA / SMK', 'Laki', 'Menjadi Kepala Bidang Prakerin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `perusahaan`
--

CREATE TABLE IF NOT EXISTS `perusahaan` (
  `perusahaan_id` int(10) NOT NULL AUTO_INCREMENT,
  `perusahaan_nama` varchar(30) NOT NULL,
  `perusahaann_alamat` text NOT NULL,
  `perusahaan_nopon` int(20) NOT NULL,
  `perusahaan_website` varchar(30) DEFAULT NULL,
  `perusahaan_tentang` text NOT NULL,
  `perusahaan_username` varchar(30) NOT NULL,
  `perusahaan_password` varchar(30) NOT NULL,
  `perusahaan_logo` varchar(100) NOT NULL,
  PRIMARY KEY (`perusahaan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data untuk tabel `perusahaan`
--

INSERT INTO `perusahaan` (`perusahaan_id`, `perusahaan_nama`, `perusahaann_alamat`, `perusahaan_nopon`, `perusahaan_website`, `perusahaan_tentang`, `perusahaan_username`, `perusahaan_password`, `perusahaan_logo`) VALUES
(12, 'Kodeku.Net', 'Jalan Simpang Tondano Barat IV , Nomor A3F3 , Sawojajar, Malang.', 1213423421, 'kodeku.net', 'Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat-kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.\r\n', 'kodeku', 'kodeku', 'download.jpg'),
(14, 'SMK PGRI 3 Malang', 'Tlogomas Malang', 321456876, 'skariga.com', 'skariga skariga skariga skariga skariga skariga skariga skariga skariga skariga', 'skariga', 'skariga', 'sa.PNG'),
(15, 'Jawa Timur Park 3', 'Jl. Raya Ir. Soekarno No.144, Beji, Junrejo, Kota Batu, Jawa Timur 65236.', 2147483647, 'jatimpark3dino.com', 'Menurut Kamus Besar Bahasa Indonesia, pengertian wisata adalah bepergian secara bersama-sama dengan tujuan untuk bersenang-senang, menambah pengetahuan, dan lain-lain. Selain itu juga dapat diartikan sebagai bertamasya atau piknik.', 'asd', 'asd', 'sa1.PNG'),
(16, 'PT.Indonesia Berkarya', 'Perum Taman Suko Asri Blok BB No. 9, Suko, Sukodono, Suko, Sukodono, Suko, Sukodono, Sidoarjo Regency, East Java 61258.', 234542, 'Indonesiakarya.com', 'Perusahaan adalah tempat terjadinya kegiatan produksi dan berkumpulnya semua faktor produksi. Setiap perusahaan ada yang terdaftar di pemerintah dan ada pula yang tidak. Bagi perusahaan yang terdaftar di pemerintah, mereka mempunyai badan usaha untuk perusahaannya.', 'karya', 'karya', 'WIN_20170325_162630.JPG'),
(17, 'TB.Sumber Suko', 'Jl. Joyosuko Timur No.1A, Merjosari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144.', 122345325, 'tbsmsko.com', 'Bangunan adalah struktur buatan manusia yang terdiri atas dinding dan atap yang didirikan secara permanen di suatu tempat. Bangunan juga biasa disebut dengan rumah dan gedung, yaitu segala sarana, prasarana atau infrastruktur dalam kebudayaan atau kehidupan manusia dalam membangun peradabannya.', 'tbsm', 'tbsm', 'Capturedfd.PNG'),
(18, 'Lenovo Service Center', 'Kav 1-2 No.1B, Jl. Puncak Jaya, Pisang Candi, Sukun, Kota Malang, Jawa Timur 65115', 2147483647, 'lenovo.com', 'Komputer adalah alat yang dipakai untuk mengolah data menurut prosedur yang telah dirumuskan. Kata computer pada awalnya dipergunakan untuk menggambarkan orang yang perkerjaannya melakukan perhitungan aritmetika, dengan atau tanpa alat bantu, tetapi arti kata ini kemudian dipindahkan kepada mesin itu sendiri.', 'lenovo', 'lenovo', 'Capture.PNG');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_nama` varchar(30) NOT NULL,
  `jenis_kelamin` enum('Laki','Perempuan') NOT NULL,
  `user_nopon` int(20) NOT NULL,
  `user_alamat` text NOT NULL,
  `user_username` varchar(30) NOT NULL,
  `user_password` varchar(30) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`user_id`, `user_nama`, `jenis_kelamin`, `user_nopon`, `user_alamat`, `user_username`, `user_password`) VALUES
(22, 'Febri Fahturrohman Saleh', 'Laki', 2147483647, 'Desa Junrejo Kota Batu', 'febri', 'febri'),
(23, 'Aditya', 'Perempuan', 2147483647, 'beji', 'adit', 'adit'),
(24, 'Shafa Safira', 'Perempuan', 2147483647, 'Sengkaling', 'shafaa', 'shafasfr'),
(25, 'Dwi Ayu', 'Perempuan', 1234566789, 'Jombok Ngantang', 'ayu', 'ayu');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `melamar`
--
ALTER TABLE `melamar`
  ADD CONSTRAINT `melamar_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `melamar_ibfk_2` FOREIGN KEY (`pekerjaan_id`) REFERENCES `pekerjaan` (`pekerjaan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `melamar_ibfk_3` FOREIGN KEY (`perusahaan_id`) REFERENCES `perusahaan` (`perusahaan_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
