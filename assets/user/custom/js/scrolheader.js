<script type="text/javascript">
  var getHeaderHeight = $('.headerContainerWrapper').outerHeight();
  var borderAmount = 2;
  var shadowAmount = 30
  var lastScrollPosition = 0;
  $('.headerContainerWrapper').css('top', '-' + (getHeaderHeight + shadowAmount + borderAmount) + 'px');

  $(window).scroll(function() {
    var currentScrollPosition = $(window).scrollTop();

    if ($(window).scrollTop() > 2 * (getHeaderHeight + shadowAmount + borderAmount) ) {

      $('body').addClass('scrollActive').css('padding-top', getHeaderHeight);
      $('.headerContainerWrapper').css('top', 0);

      if (currentScrollPosition < lastScrollPosition) {
        $('.headerContainerWrapper').css('top', '-' + (getHeaderHeight + shadowAmount + borderAmount) + 'px');
      }
      lastScrollPosition = currentScrollPosition;

    } else {
      $('body').removeClass('scrollActive').css('padding-top', 0);
    }
  });
</script>