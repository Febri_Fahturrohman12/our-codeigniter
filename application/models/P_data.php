<?php 
 
class P_data extends CI_Model{
	function tampil_data(){
		return $this->db->get('user');
	}

	function addpekerjaan($data,$table){
		$this->db->insert($table,$data);
	}
	function getid($id)
	{
		$this->db->where('perusahaan_id',$perusahaan_id);
		return $this->db->get('perusahaan');
	}
	function ambil_datapp($perpage,$offset){
        return $this->db
        				->get_where("pekerjaan",
        					array('perusahaan_id' => $this->session->userdata('perusahaan_id')),
        					$perpage,$offset)->result();
    }
    function edit_pekerjaan($where,$table){		
		return $this->db->get_where($table,$where);
	}
	function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
    function hapus_kerja($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
	function ambil_where($where,$table){
        // return $this->db->get_where($table,$where);
		$this->db->select('pekerjaan.*, perusahaan.perusahaan_id, perusahaan.perusahaan_nama, perusahaan.perusahaann_alamat,perusahaan.perusahaan_tentang,perusahaan.perusahaan_nopon,perusahaan.perusahaan_website'); //mengambil semua data dari tabel data_users dan users
    	$this->db->from('pekerjaan'); //dari tabel data_users
    	$this->db->join('perusahaan', 'perusahaan.perusahaan_id = pekerjaan.perusahaan_id', 'left'); //menyatukan tabel users menggunakan left join
    	$data = $this->db->get_where($table,$where); //mengambil seluruh data
    	return $data->result(); //mengembalikan data
    }
}