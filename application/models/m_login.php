<?php 
 
class M_login extends CI_Model{	
	//LOGIN//
	function cek_loginuser($user_username, $user_password)
	{		
		// $this->db->query('select * from users where username='$username'');
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where(['user_username'=>$user_username]);// tanda [] ini sama dengan fungsi array
		$return = $this->db->get();
		// memeriksa datanya apakah ada atau tidak
		// jika ada maka akan menampilkan pesan sukses
		if ($return->num_rows() > 0) 
		{
			// result sama funginya dennga mysql fetch array mengambil nilainya kembali
			foreach ($return->result() as $row) 
			{
				// sebelumnya kita ambil data usernya dulu untuk bisa kita tampilkan
						// ambil data user dari table users berdasarkan usernamennya
						$dataUsers = $this->db->get_where('user', ['user_username'=>$user_username]);
						// result ini fungsinya tampilkan hasilnya dari datanya users kemudian gantikan variabel datausers dengan variabel data
						foreach ($dataUsers->result() as $data) 
						{
							$session = array('status' => "login",
											'user_username'=>$data->user_username,
											'user_password'=>$data->user_password,
											'user_id'=>$data->user_id,
											'user_nama'=>$data->user_nama);
									$this->session->set_userdata( $session);
						}
						// jika semua fungsi di atas berjalan dengan lancar makka arahkan halaman ke c_mahasiswa
						redirect('User','refresh');
			}
		}
		// jika gagal
		else
		{
			$this->load->view('errorlogin');
		}
	}


	function cek_loginp($perusahaan_username, $perusahaan_password){		
		// $this->db->query('select * from users where username='$username'');
		$this->db->select('*');
		$this->db->from('perusahaan');
		$this->db->where(['perusahaan_username'=>$perusahaan_username]);// tanda [] ini sama dengan fungsi array
		$return = $this->db->get();
		// memeriksa datanya apakah ada atau tidak
		// jika ada maka akan menampilkan pesan sukses
		if ($return->num_rows() > 0) 
		{
			// result sama funginya dennga mysql fetch array mengambil nilainya kembali
			foreach ($return->result() as $row) 
			{
				// sebelumnya kita ambil data usernya dulu untuk bisa kita tampilkan
						// ambil data user dari table users berdasarkan usernamennya
						$dataUsers = $this->db->get_where('perusahaan', ['perusahaan_username'=>$perusahaan_username]);
						// result ini fungsinya tampilkan hasilnya dari datanya users kemudian gantikan variabel datausers dengan variabel data
						foreach ($dataUsers->result() as $data) 
						{
							$session = array('status' => "login",
											'perusahaan_id'=>$data->perusahaan_id,
											'perusahaan_username'=>$data->perusahaan_username,
											'perusahaan_password'=>$data->perusahaan_password,
											'perusahaan_nama'=>$data->perusahaan_nama,
											'perusahaan_logo'=>$data->perusahaan_logo);
									$this->session->set_userdata( $session);
						}
						// jika semua fungsi di atas berjalan dengan lancar makka arahkan halaman ke c_mahasiswa
						redirect('Perusahaan','refresh');
			}
		}
		// jika gagal
		else
		{
			$this->load->view('errorloginp');
		}
	}



	//DAFTAR//
	//PERUSAHAAN//
	// Fungsi untuk menampilkan semua data gambar
	public function view(){
		return $this->db->get('gambar')->result();
	}
	
	// Fungsi untuk melakukan proses upload file
	public function upload(){
		$config['upload_path'] = './assets/logoperusahaan/';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']	= '2048';
		$config['remove_space'] = TRUE;
	
		$this->load->library('upload', $config); // Load konfigurasi uploadnya
		if($this->upload->do_upload('input_gambar')){ // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		}else{
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}
	
	// Fungsi untuk menyimpan data ke database
	public function save($upload){
		$data = array(
			'perusahaan_nama'=>$this->input->post('perusahaan_nama'),
			'perusahaann_alamat'=>$this->input->post('perusahaan_alamat'),
			'perusahaan_nopon'=>$this->input->post('perusahaan_nopon'),
			'perusahaan_website'=>$this->input->post('perusahaan_website'),
			'perusahaan_tentang'=>$this->input->post('perusahaan_tentang'),
			'perusahaan_username'=>$this->input->post('perusahaan_username'),
			'perusahaan_password'=>$this->input->post('perusahaan_password'),
			'perusahaan_logo' => $upload['file']['file_name']
			
		);
		
		$this->db->insert('perusahaan', $data);
	}


	//PELAMAR//
	function tampil_data(){
		return $this->db->get('user');
	}
	function input_datauser($data,$table){
		$this->db->insert($table,$data);
	}
}




