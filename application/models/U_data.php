<?php 
 
class U_data extends CI_Model{
	function tampil_data(){
		return $this->db->get('user');
	}
    function ambil_perusahaan($tabel){
        return $this->db->get($tabel);
    }
    function ambil_datap($perpage,$offset){
        return $this->db->get("perusahaan",$perpage,$offset)->result();
    }
    function ambil_datapp($perpage,$offset){
        return $this->db->get("pekerjaan",$perpage,$offset)->result();
    }
    function ambil_wherenama($where,$table){
        return $this->db->get_where($table,$where);

    }
    function ambil_wherek($where,$table){
        return $this->db->get_where($table,$where);

    }

    function ambil_whereerr($where,$table){
        return $this->db->get_where($table,$where);

    }
     function ambil_whereerrr($where,$table){
        return $this->db->get_where($table,$where);

    }
    function input_lamaran($data,$table){
        $this->db->insert($table,$data);
    }
    function ambil_where($where,$table){
        // return $this->db->get_where($table,$where);
        $this->db->select('pekerjaan.*, perusahaan.perusahaan_id, perusahaan.perusahaan_nama, perusahaan.perusahaann_alamat,perusahaan.perusahaan_tentang,perusahaan.perusahaan_nopon,perusahaan.perusahaan_website'); //mengambil semua data dari tabel data_users dan users
        $this->db->from('pekerjaan'); //dari tabel data_users
        $this->db->join('perusahaan', 'perusahaan.perusahaan_id = pekerjaan.perusahaan_id', 'left'); //menyatukan tabel users menggunakan left join
        $data = $this->db->get_where($table,$where); //mengambil seluruh data
        return $data->result(); //mengembalikan data

    }
    function ambil_whereper($where,$table){
        return $this->db->get_where($table,$where);
        // $this->db->select('pekerjaan.*, perusahaan.perusahaan_id, perusahaan.perusahaan_nama, perusahaan.perusahaann_alamat,perusahaan.perusahaan_tentang,perusahaan.perusahaan_nopon,perusahaan.perusahaan_website'); //mengambil semua data dari tabel data_users dan users
        // $this->db->from('pekerjaan'); //dari tabel data_users
        // $this->db->join('perusahaan', 'perusahaan.perusahaan_id = pekerjaan.perusahaan_id', 'left'); //menyatukan tabel users menggunakan left join
        // $data = $this->db->get_where($table,$where); //mengambil seluruh data
        // return $data->result(); //mengembalikan data

    }
    // public function upload(){
        // $config['upload_path'] = './assets/user/custom/berkas/';
        // $config['allowed_types'] = 'jpg|png|jpeg';
        // $config['max_size'] = '2048';
        // $config['remove_space'] = TRUE;
    
        // $this->load->library('upload', $config); // Load konfigurasi uploadnya
        // if($this->upload->do_upload('user_cv')){ // Lakukan upload dan Cek jika proses upload berhasil
        //     // Jika berhasil :
        //     $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
        //     return $return;
        // }else{
        //     // Jika gagal :
        //     $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
        //     return $return;
        // }


// }
    // Fungsi untuk melakukan proses upload file
    public function upload(){
        $config['upload_path'] = './assets/logoperusahaan/';
        $config['allowed_types'] = 'pdf|txt|docx';
        $config['max_size'] = '2048';
        $config['remove_space'] = TRUE;
    
        $this->load->library('upload', $config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('user_cv')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }
    
    // Fungsi untuk menyimpan data ke database
    public function save($upload){
        $user_id = $this->input->post('user_id');
        // $pekerjaan_id = $this->input->post('pekerjaan_id');
        // $perusahaan_id = $this->input->post('perusahaan_id');
        // $user_nama = $this->input->post('user_nama');        
        // $user_cv = $this->input->post('user_cv');
        // $pekerjaan_nama = $this->input->post('pekerjaan_nama');
        // $perusahaan_nama = $this->input->post('perusahaan_nama');
        // $user_email = $this->input->post('user_email');
        // $user_nopon = $this->input->post('user_nopon');
        // $perusahaan_nopon = $this->input->post('perusahaan_nopon');

        $data = array(
            'user_id' => $this->input->post('user_id'),
            'pekerjaan_id' => $this->input->post('pekerjaan_id'),
            'perusahaan_id' => $this->input->post('perusahaan_id'),
            'user_nama' => $this->input->post('user_nama'),            
            'pekerjaan_nama' => $this->input->post('pekerjaan_nama'),
            'perusahaan_nama' => $this->input->post('perusahaan_nama'),
            'user_email' => $this->input->post('user_email'),
            'user_nopon' => $this->input->post('user_nopon'),
            'perusahaan_nopon' => $this->input->post('perusahaan_nopon'),
            'user_cv' => $upload['file']['file_name']
            
        );
        
        $this->db->insert('melamar', $data);
    }
// $data = array(
            
//             'user_id' => $this->input->post('user_id'),
//             'pekerjaan_id' => $this->input->post('pekerjaan_id'),
//             'perusahaan_id' => $this->input->post('perusahaan_id'),
//             'user_nama' => $this->input->post('user_nama'),            
//             'pekerjaan_nama' => $this->input->post('pekerjaan_nama'),
//             'perusahaan_nama' => $this->input->post('perusahaan_nama'),
//             'user_email' => $this->input->post('user_email'),
//             'user_nopon' => $this->input->post('user_nopon'),
//             'perusahaan_nopon' => $this->input->post('perusahaan_nopon'),
//             'user_cv' => $upload['file']['file_name']
//         );
//         
}