<?php 
 
class User extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url('pertama/'));
		}
		$this->load->model('U_data');
	}
 	function index()
 	{
 		
 		$this->load->view('user/index');

	}

	public function pekerjaan($offset=0){
		$data_perusahaan = $this->db->get("pekerjaan");

		$config['total_rows']= $data_perusahaan->num_rows();
 		$config['base_url']= base_url(). 'user/pekerjaan/';
 		$config['per_page']=5;

 		//Konfigurai class bootstrap
 		$config['full_tag_open']="<ul class='pagination pagination-sm' style='position:relative;'>";
 		$config['full_tag_close']="</ul>";
 		$config['num_tag_open']="<li>";
 		$config['num_tag_close']="</li>";
 		$config['cur_tag_open']="<li class='disabled'><li class'active'><a href='#'>";
 		$config['cur_tag_close']="<span class'sr-only'></span></a></li>";
 		$config['next_tag_open']="<li>";
 		$config['next_tag_close']="</li>";
 		$config['prev_tag_open']="<li>";
 		$config['prev_tag_close']="</li>";
 		$config['first_tag_open']="<li>";
 		$config['first_tag_close']="</li>";
 		$config['last_tag_open']="<li>";
 		$config['last_tag_close']="</li>";

 		$this->pagination->initialize($config);
 		$data['halaman']=$this->pagination->create_links();
 		$data['offset']=$offset;

 		$data['data']=$this->U_data->ambil_datapp($config['per_page'],$offset);

		$this->load->view('user/pekerjaan',$data);
	}
 	public function perusahaan($offset=0){
 		$data_perusahaan = $this->db->get("perusahaan");

 		$config['total_rows']= $data_perusahaan->num_rows();
 		$config['base_url']= base_url(). 'user/perusahaan/';
 		$config['per_page']=4;

 		//Konfigurai class bootstrap
 		$config['full_tag_open']="<ul class='pagination pagination-sm' style='position:relative;'>";
 		$config['full_tag_close']="</ul>";
 		$config['num_tag_open']="<li>";
 		$config['num_tag_close']="</li>";
 		$config['cur_tag_open']="<li class='disabled'><li class'active'><a href='#'>";
 		$config['cur_tag_close']="<span class'sr-only'></span></a></li>";
 		$config['next_tag_open']="<li>";
 		$config['next_tag_close']="</li>";
 		$config['prev_tag_open']="<li>";
 		$config['prev_tag_close']="</li>";
 		$config['first_tag_open']="<li>";
 		$config['first_tag_close']="</li>";
 		$config['last_tag_open']="<li>";
 		$config['last_tag_close']="</li>";

 		$this->pagination->initialize($config);
 		$data['halaman']=$this->pagination->create_links();
 		$data['offset']=$offset;

 		$data['data']=$this->U_data->ambil_datap($config['per_page'],$offset);

 		// $data['dataperusahaan']= $this->U_data->ambil_perusahaan('perusahaan')->result();
		$this->load->view('user/perusahaan',$data);
	}
	

	public function readperusahaan($id){
		$where= array('perusahaan_id' => $id);

		$data['perusahaanread']= $this->U_data->ambil_whereper($where,'perusahaan')->result();
		$this->load->view('user/profilperusahaan',$data);
	}

	public function readpekerjaan($id){
		$where= array('pekerjaan_id' => $id);

		$data['pekerjaanread']= $this->U_data->ambil_where($where,'');
		$this->load->view('user/profilkerja',$data);
	}

	public function resumekerjaa($id){
		$where= array('pekerjaan_id' => $id);

		$data['resumekerjaa']= $this->U_data->ambil_wherek($where,'pekerjaan')->result();
		$this->load->view('user/resumekerja',$data);
	}

	public function tentang(){
		$this->load->view('user/tentang');
	}
	public function kontak(){
		$this->load->view('user/kontak');
	}
	public function akun(){
		$this->load->view('user/akun');
	}
	public function profilperusahaan($id){
		$where= array('perusahaan_id' => $id);

		$data['perusahaanread']= $this->U_data->ambil_whereper($where,'perusahaan')->result();
		$this->load->view('user/profilperusahaan',$data);
	}
	public function profilkerja(){
		$this->load->view('user/profilkerja');
	}
	public function resumekerja($id){
		$this->load->view('user/resumekerja');
	}

	function lamarkerja($id){

		// $user_id = $this->input->post('user_id');
		// $pekerjaan_id = $this->input->post('pekerjaan_id');
		// $perusahaan_id = $this->input->post('perusahaan_id');
		// $user_nama = $this->input->post('user_nama');		
		// $user_cv = $this->input->post('user_cv');
		// $pekerjaan_nama = $this->input->post('pekerjaan_nama');
		// $perusahaan_nama = $this->input->post('perusahaan_nama');
		// $user_email = $this->input->post('user_email');
		// $user_nopon = $this->input->post('user_nopon');
		// $perusahaan_nopon = $this->input->post('perusahaan_nopon');

		// $data = array(
		// 	'user_id' => $user_id,
		// 	'pekerjaan_id' => $pekerjaan_id,
		// 	'perusahaan_id' => $perusahaan_id,
		// 	'user_nama' => $user_nama,
		// 	'user_cv' => $user_cv,
		// 	'pekerjaan_nama' => $pekerjaan_nama,
		// 	'perusahaan_nama' => $perusahaan_nama,
		// 	'user_email' => $user_email,
		// 	'user_nopon' => $user_nopon,
		// 	'perusahaan_nopon' => $perusahaan_nopon,
			
		// 	);
		// $this->U_data->input_lamaran($data,'melamar');
		$where= array('perusahaan_id' => $id);

		$data = array();
		
		if($this->input->post('submit')){ // Jika user menekan tombol Submit (Simpan) pada form
			// lakukan upload file dengan memanggil function upload yang ada di GambarModel.php
			$upload = $this->U_data->upload();
			
			if($upload['result'] == "success"){ // Jika proses upload sukses
				 // Panggil function save yang ada di GambarModel.php untuk menyimpan data ke database
				$this->U_data->save($upload);
				
				redirect('user/suksess'); // Redirect kembali ke halaman awal / halaman view data
			}else{ // Jika proses upload gagal
				$data['message'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}
		// $idmelamar['lamarkerja']= $this->U_data->ambil_whereerr($where,'pekerjaan');
		$this->load->view('user/error');

	}
	function suksess(){
		$this->load->view('user/sukses');
	}
}