<?php 
 
class Perusahaan extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('P_data'); 
		if($this->session->userdata('status') != "login"){
			redirect(base_url('pertama/'));
		}
	}
 	function index()
 	{
 		$this->load->view('perusahaan/index');
  	
	}
	public function addpekerjaan(){
		$perusahaan_id = $this->session->userdata('perusahaan_id');
		$perusahaan_nama = $this->session->userdata('perusahaan_nama');
		$pekerjaan_nama = $this->input->post('nama_pekerjaan');
		$tipe_gajian = $this->input->post('tipe_gajian');
		$perusahaan_alamat = $this->input->post('perusahaan_alamat');
		$perusahaan_nopon = $this->input->post('perusahaan_nopon');
		$umur = $this->input->post('umur');
		$pendidikan = $this->input->post('pendidikan');
		$jenis_kelamin = $this->input->post('jenis_kelamin');		
		$tentang_pekerjaan = $this->input->post('tentang_pekerjaan');
 
		$data = array(
			'perusahaan_id' => $perusahaan_id,
			'perusahaan_nama' => $perusahaan_nama,
			'pekerjaan_nama' => $pekerjaan_nama,
			'tipe_gaji' => $tipe_gajian,
			'perusahaan_alamat' => $perusahaan_alamat,
			'perusahaan_nopon' => $perusahaan_nopon,
			'umur' => $umur,
			'pendidikan' => $pendidikan,
			'jenis_kelamin' => $jenis_kelamin,
			'tentang_pekerjaan' => $tentang_pekerjaan
			);
		$this->P_data->addpekerjaan($data,'pekerjaan');
		redirect('perusahaan/pekerjaan');
	}

	public function pekerjaan($offset=0){
		$data_perusahaan = $this->db->get("pekerjaan");

		$config['total_rows']= $data_perusahaan->num_rows();
 		$config['base_url']= base_url(). 'perusahaan/pekerjaan/';
 		$config['per_page']=10;

 		//Konfigurai class bootstrap
 		$config['full_tag_open']="<ul class='pagination pagination-sm' style='position:relative;'>";
 		$config['full_tag_close']="</ul>";
 		$config['num_tag_open']="<li>";
 		$config['num_tag_close']="</li>";
 		$config['cur_tag_open']="<li class='disabled'><li class'active'><a href='#'>";
 		$config['cur_tag_close']="<span class'sr-only'></span></a></li>";
 		$config['next_tag_open']="<li>";
 		$config['next_tag_close']="</li>";
 		$config['prev_tag_open']="<li>";
 		$config['prev_tag_close']="</li>";
 		$config['first_tag_open']="<li>";
 		$config['first_tag_close']="</li>";
 		$config['last_tag_open']="<li>";
 		$config['last_tag_close']="</li>";

 		$this->pagination->initialize($config);
 		$data['halaman']=$this->pagination->create_links();
 		$data['offset']=$offset;

 		$data['data']=$this->P_data->ambil_datapp($config['per_page'],$offset);

		$this->load->view('perusahaan/pekerjaan',$data);
	}
	
 	public function pelamar(){
		$this->load->view('perusahaan/pelamar');
	}
	public function tentang(){
		$this->load->view('perusahaan/tentang');
	}
	public function kontak(){
		$this->load->view('perusahaan/kontak');
	}
	public function akun(){
		$this->load->view('perusahaan/akun');
	}
	public function Editkerja(){
		$this->load->view('perusahaan/editkerja');
	}
	public function profilpelamar(){
		$this->load->view('perusahaan/profilpelamar');
	}
	public function lihatsebagai($id){
		$where= array('pekerjaan_id' => $id);

		$data['pekerjaanread']= $this->P_data->ambil_where($where,'');

		$this->load->view('perusahaan/lihatsebagai',$data);
	}

	
	public function hapuskerja($pekerjaan_id){
		$where = array('pekerjaan_id' => $pekerjaan_id);
		$this->P_data->hapus_kerja($where,'pekerjaan');
		redirect('perusahaan/pekerjaan');
	}

	public function edit($pekerjaan_id){
		$where = array('pekerjaan_id' => $pekerjaan_id);
		$data['pekerjaan'] = $this->P_data->edit_pekerjaan($where,'pekerjaan')->result();
		$this->load->view('perusahaan/editkerja',$data);
	}
	public function update(){

		$pekerjaan_id = $this->input->post('pekerjaan_id');
		$perusahaan_nama = $this->input->post('perusahaan_nama');
		$pekerjaan_nama = $this->input->post('nama_pekerjaan');
		$tipe_gajian = $this->input->post('tipe_gajian');
		$perusahaan_alamat = $this->input->post('perusahaan_alamat');
		$perusahaan_nopon = $this->input->post('perusahaan_nopon');
		$umur = $this->input->post('umur');
		$pendidikan = $this->input->post('pendidikan');
		$jenis_kelamin = $this->input->post('jenis_kelamin');		
		$tentang_pekerjaan = $this->input->post('tentang_pekerjaan');
	 
		$data = array(
			'perusahaan_nama' => $perusahaan_nama,
			'pekerjaan_nama' => $pekerjaan_nama,
			'tipe_gaji' => $tipe_gajian,
			'perusahaan_alamat' => $perusahaan_alamat,
			'perusahaan_nopon' => $perusahaan_nopon,
			'umur' => $umur,
			'pendidikan' => $pendidikan,
			'jenis_kelamin' => $jenis_kelamin,
			'tentang_pekerjaan' => $tentang_pekerjaan
		);
	 
		$where = array(
			'pekerjaan_id' => $pekerjaan_id
		);
	 
		$this->P_data->update_data($where,$data,'pekerjaan');
		redirect('Perusahaan/pekerjaan');
		}
}