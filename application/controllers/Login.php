<?php 
 
class Login extends CI_Controller{
 
	function __construct(){
		parent::__construct();		
		$this->load->model('m_login');
 
	}
 
	function index(){
		$data['gambar'] = $this->m_login->view();
		$data['user'] = $this->m_login->tampil_data()->result();
		$this->load->view('login');
	}
	function suksess(){
		$data['user'] = $this->m_login->tampil_data()->result();
		$this->load->view('sukses',$data);
	}


 	//LOGIN//
	function aksi_loginuser(){
		
		$this->form_validation->set_rules('user_username', 'user_username' , 'required');
		$this->form_validation->set_rules('user_password', 'user_password' , 'required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">','</span>');
		if ($this->form_validation->run() ) 
		{
			$user_username = $this->input->post('user_username');//sesuaikan nama fiednya denagn inputan ok
			$user_password = $this->input->post('user_password');
			$this->m_login->cek_loginuser($user_username, $user_password);
			
		}
		 else
		{
			$this->load->view('errorlogin');
		}
	}
 	function aksi_loginp(){
		
		$this->form_validation->set_rules('perusahaan_username', 'perusahaan_username' , 'required');
		$this->form_validation->set_rules('perusahaan_password', 'perusahaan_password' , 'required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">','</span>');
		if ($this->form_validation->run() ) 
		{
			$perusahaan_username = $this->input->post('perusahaan_username');//sesuaikan nama fiednya denagn inputan ok
			$perusahaan_password = $this->input->post('perusahaan_password');
			$this->m_login->cek_loginp($perusahaan_username, $perusahaan_password);
			
		}
		 else
		{
			$this->load->view('errorlogin');
		}
	}



	//DAFTAR//
	public function daftarperusahaan(){
		$data = array();
		
		if($this->input->post('submit')){ // Jika user menekan tombol Submit (Simpan) pada form
			// lakukan upload file dengan memanggil function upload yang ada di GambarModel.php
			$upload = $this->m_login->upload();
			
			if($upload['result'] == "success"){ // Jika proses upload sukses
				 // Panggil function save yang ada di GambarModel.php untuk menyimpan data ke database
				$this->m_login->save($upload);
				
				redirect('login/suksess'); // Redirect kembali ke halaman awal / halaman view data
			}else{ // Jika proses upload gagal
				$data['message'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}
		
		$this->load->view('daftarperusahaan', $data);
	}


	//PELAMAR//
	function daftaruser(){
		$user_nama = $this->input->post('user_nama');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$user_nopon = $this->input->post('user_nopon');
		$user_alamat = $this->input->post('user_alamat');
		$user_username = $this->input->post('user_username');
		$user_password = $this->input->post('user_password');
 
		$data = array(
			'user_nama' => $user_nama,
			'jenis_kelamin' => $jenis_kelamin,
			'user_nopon' => $user_nopon,
			'user_alamat' => $user_alamat,
			'user_username' => $user_username,
			'user_password' => $user_password
			);
		$this->m_login->input_datauser($data,'user');
		redirect('login/suksess');

	}



	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('pertama/'));
	}
}