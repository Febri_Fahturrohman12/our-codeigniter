<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Pertama extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
	}
 
	public function index(){
		$this->load->view('index');
	}
	public function admin(){
		$data = array();
		$this->template->load('admin','dashboard', $data);
	}
 	public function pekerjaan(){
		$this->load->view('pekerjaan');
	}
 	public function perusahaan(){
		$this->load->view('perusahaan');
	}
	public function tentang(){
		$this->load->view('tentang');
	}
	public function kontak(){
		$this->load->view('kontak');
	}
	public function daftar(){
		$this->load->view('daftar');
	}
	public function masuk(){
		$this->load->view('login');
	}
	public function daftaruser(){
		$this->load->view('daftaruser');
	}
	public function daftarperusahaan(){
		$this->load->view('daftarperusahaan');
	}
	public function loginuser(){
		$this->load->view('loginuser');
	}
	public function loginperusahaan(){
		$this->load->view('loginperusahaan');
	}
	public function upload(){
		$this->load->view('form');
	}
}