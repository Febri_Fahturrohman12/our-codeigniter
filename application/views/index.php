<!doctype html>
 <html lang="en">
   <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>LOKER</title>
 
     <!-- Bootstrap -->
     <link href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
 
     <!-- HTML5 shim and Respond.js for IE8 support of 
          HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
 
    <!-- Custom CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" href="assets/custom/css/font-awesome.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/custom/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash|Courgette|Quicksand:300|Saira+Semi+Condensed" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/custom/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/custom/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/custom/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/custom/css/slick-theme.css">
    <style type="text/css">
        html, body {
          margin: 0;
          padding: 0;
        }

        * {
          box-sizing: border-box;
        }

        .slider {
            width: 100%;
            margin: 0px auto;
        }

        .slick-slide {
          margin: 0px 20px;
        }

        .slick-slide img {
          width: 100%;
          background-size: cover;
        }

        .slick-prev:before,
        .slick-next:before {
          color: black;
        }


        .slick-slide {
          transition: all ease-in-out .3s;
          opacity: .2;
        }
        
        .slick-active {
          opacity: 1;
        }

        .slick-current {
          opacity: 1;
        }
    </style>
    <link href="<?php echo base_url() ?>assets/custom/css/style.css" rel="stylesheet">
 
   </head>
   <body>
    <div class="container-fluid" style="margin: 0px;padding: 0px;">
      <!-- Bagian Header -->
      <div class="abc col-lg-12 col-md-12 col-sm-12 col-xs-12" id="site-header" >
          <header>
            <div class="col-lg-12 col-md-12 col-xs-12 sosmed header" id="site-header" style="height: 40px; max-height: 40px;background-color: rgba(0,0,0,0.26);">
              <div class="col-lg-12 col-xs-12 col-md-12">
                <ul class="sosmed nav navbar-nav navbar-right">
                  <li><a href="#" class="sos"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="sos"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="sos"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="sos"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                </ul>
              </div>
            </div>
            <nav class="navbar navbar-default col-lg-12 headerContainerWrapper" style="background-color: rgba(0,0,0,0.26); padding-top: 10px;padding-bottom: 10px; border: none; border-top: 0.1px solid #848484;  border-radius: 0px;">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" style="color: #fff;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>

                  <a class="navbar-brand" style="color: #fff;font-weight: bold;font-family: 'Saira Semi Condensed', sans-serif;" href="#"><span class="glyphicon glyphicon-send" aria-hidden="true">&nbsp</span> Lowongan Kerja</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo site_url('pertama/pekerjaan')?>">Pekerjaan <span class="sr-only">(current)</span></a></li>
                    <li><a href="<?php echo site_url('pertama/perusahaan')?>">Perusahaan</a></li>
                    <li><a href="<?php echo site_url('pertama/tentang')?>">Tentang Kami</a></li>
                    <li><a href="<?php echo site_url('pertama/kontak')?>">Kontak</a></li>
                    <li><a href="<?php echo site_url('pertama/daftar')?>"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Daftar</a></li>
                    <li><a href="<?php echo site_url('pertama/masuk')?>"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Masuk</a></li>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>
          </header>
          <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12 subhead" style="padding-top: 20px; padding-left: 50px;display: inline-grid;">
                            <h1 style="color: #fff; font-weight: bold;font-size: 42px; text-shadow: 1px 1px 3px #000;">Lowongan Pekerjaan</h1>
                            <p style="color: #ddd; font-weight: normal;font-size: 20px; text-shadow: 1px 1px 3px #000;">Website Resmi Tentang Lowongan Pekerjaan Sekitar Kota Batu</p>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 subhead" style="padding-top: 20px; padding-left: 50px; padding-bottom: 40px;">
          <h3 style="font-style: normal;font-weight: bold; font-size: 20px; color: #fff; padding-bottom: 50px;">CARI PEKERJAAN &nbsp <a href="<?php echo site_url('pertama/masuk')?>"><button type="button" class="btn btn-warning">Selengkapnya ></button></a></h3>
      </div>
    </div>
    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-xs-12" style="margin-top: 30px;">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="lokasi col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                                           <span><i class="fa fa-globe wow  slideInLeft" aria-hidden="true" style="font-size: 74px;color: #f3a913;margin-bottom: 5px; animation-duration: 2s;"></i></span>
                                           <p style="color: #bf3654; font-size: 22px; margin-top: 15px; font-weight: bold;">950</p>
                                           <p style="margin-top: -5px; font-size: 18px;font-family: 'Saira Semi Condensed', sans-serif;">Daerah Kota Batu</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                       <div class="lokasi col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                                            <span><i class="fa fa-pie-chart wow  slideInLeft" aria-hidden="true" style="font-size: 74px;color: #f3a913;margin-bottom: 5px;animation-duration: 3s;"></i></span>
                                            <p style="color: #bf3654; font-size: 22px; margin-top: 15px; font-weight: bold;">1152</p>
                                            <p style="margin-top: -5px; font-size: 18px;font-family: 'Saira Semi Condensed', sans-serif;">Jumlah Operator</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="lokasi col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                                            <span><i class="fa fa-usd wow  slideInRight" aria-hidden="true" style="font-size: 74px;color: #f3a913;margin-bottom: 5px; animation-duration: 3s;"></i></span>
                                            <p style="color: #bf3654; font-size: 22px; margin-top: 15px; font-weight: bold;">1480</p>
                                            <p style="margin-top: -5px; font-size: 18px;font-family: 'Saira Semi Condensed', sans-serif;">Total Keuntungan</p>
                                        </div>
                                   </div>
                                   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="lokasi col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                                            <span><i class="fa fa-comments-o wow  slideInRight" aria-hidden="true" style="font-size: 74px;color: #f3a913;margin-bottom: 5px;animation-duration: 2s;"></i></span>
                                            <p style="color: #bf3654; font-size: 22px; margin-top: 15px; font-weight: bold;">1963</p>
                                            <p style="margin-top: -5px; font-size: 18px;font-family: 'Saira Semi Condensed', sans-serif;">Customer Sukses</p>
                                        </div>
                                   </div>
                                </div>
                            </div>
                        </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 60px;">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="lok col-lg-7 col-md-6 col-sm-12 col-xs-12" style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                <div class="row">
                  <div  class="whatloker col-lg-12 col-md-12 col-sm-12 col-xs-12 wow slideInLeft" style="margin-top: 0px;padding-top: 30px;padding-bottom: 30px;animation-duration: 3s;">
                    <span>
                      <div class="row">
                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="text-align: left;">
                            <div class="lingkar" style="border:3px solid #467fd6;width:75px;height:75px;border-radius:100px;text-align: center;">
                              <h2 style="color: #fff;"><i class="fa fa-search" aria-hidden="true"></i></h2>
                            </div>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 " style=" text-align: left;">
                            <h3 style="color: #fff;">CARI PEKERJAAN SEKARANG !</h3>
                          </div>
                          <div class="col-lg-2 col-md-2 col-sm-2 " style="text-align: center;">
                            <div class="lingkar">
                              <a href="<?php echo site_url('pertama/masuk')?>"> <h2 style="color: #fff;"><i class="fa fa-arrow-right" aria-hidden="true"></i></h2></a>
                            </div>
                          </div>
                      </div>
                    </span>
                  </div>
                  <div class="whatloker col-lg-12 col-md-12 col-sm-12 col-xs-12 wow slideInLeft" style="margin-top: 35px;padding-top: 30px;padding-bottom: 30px;animation-duration: 4s;">
                    <span>
                      <div class="row">
                          <div class="col-lg-2 col-md-2 col-sm-2" style="text-align: left;">
                            <div class="lingkar" style="border:3px solid #467fd6;width:75px;height:75px;border-radius:100px;text-align: center;">
                              <h2 style="color: #fff;"><i class="fa fa-usd" aria-hidden="true"></i></h2>
                            </div>
                          </div>
                          <div class="col-lg-8 col-md-8 col-sm-8" style="text-align: left;">
                            <h3 style="color: #fff;">LIHAT PENGHASILAN </h3>
                          </div>
                          <div class="col-lg-2 col-md-2 col-sm-2" style="text-align: center;">
                            <div class="lingkar">
                              <a href="<?php echo site_url('pertama/masuk')?>"> <h2 style="color: #fff;"><i class="fa fa-arrow-right" aria-hidden="true"></i></h2></a>
                            </div>
                          </div>
                      </div>
                    </span>
                  </div>
                </div>
              </div>
              <div class="lok col-lg-5 col-md-6 col-sm-12 col-xs-12 " style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                  <div class="deskrip col-lg-12 col-md-12 col-sm-12 col-xs-12 wow pulse" style="padding-top: 0px;padding-bottom: 0px;margin-top: 0px; animation-duration: 4s;">
                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h1 style="font-weight: bold;color: #000; margin-top: 0px;margin-bottom: 40px;">PEKERJAAN DI KOTA BATU</h1>
                        <p style="color: #bf3654; font-size: 22px; margin-top: 15px; font-weight: bold;">Lamar Pekerjaan Sekarang</p>
                        <p style="margin-top: 0px;color: #4e4e4f; font-size: 18px;font-family: 'Saira Semi Condensed', sans-serif;">Tunggu apalagi? Ayo cari pekerjaan sekarang yang sesuai dengan bakat Anda.</p>
                      </div>
                    </div>
                  </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 proseskami" style="margin-top: 100px; text-align: center;">
          <h1 style="font-weight: bold;color: #000;letter-spacing: 3px;font-family: 'Roboto', sans-serif;">PROSES KAMI</h1>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 100px;">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="text-align: center;">
              <div class="lokasi pros col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                  <span>
                    <i class="fa fa-male wow  slideInLeft" aria-hidden="true" style="font-size: 74px;color: #f3a913;margin-bottom: 5px; animation-duration: 2s;"></i>
                  </span>
                  <p style="color: #bf3654; font-size: 22px; margin-top: 15px; font-weight: bold;">DAFTAR SEKARANG</p>
                  <p style="margin-top: -5px; font-size: 18px;font-family: 'Saira Semi Condensed', sans-serif;">Daftarkan diri Anda dengan mencari pekerjaan di Lowker Kota Batu.</p>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="text-align: center;"> 
              <div class="lokasi pros col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                  <span>
                    <i class="fa fa-book wow  slideInDown" aria-hidden="true" style="font-size: 74px;color: #f3a913;margin-bottom: 5px; animation-duration: 2s;"></i>
                  </span>
                  <p style="color: #bf3654; font-size: 22px; margin-top: 15px; font-weight: bold;">BUAT KESEPAKATAN</p>
                  <p style="margin-top: -5px; font-size: 18px;font-family: 'Saira Semi Condensed', sans-serif;">Penuhi syarat-syarat dokumen dan kirim ke perusahaan</p>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="text-align: center;">
              <div class="lokasi pros col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                  <span>
                    <i class="fa fa-briefcase wow  slideInRight" aria-hidden="true" style="font-size: 74px;color: #f3a913;margin-bottom: 5px; animation-duration: 2s;"></i>
                  </span>
                  <p style="color: #bf3654; font-size: 22px; margin-top: 15px; font-weight: bold;">DAPATKAN PEKERJAAN</p>
                  <p style="margin-top: -5px; font-size: 18px;font-family: 'Saira Semi Condensed', sans-serif;">Tunggu sampai perusahaan menyetujui lamaran anda melalui telepon.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 200px;padding-top: 100px;padding-bottom: 20px; background-color: #2d85bc;color: #fff;text-align: center;">
          <h1 style="font-weight: bold;letter-spacing: 2px;font-family: 'Roboto', sans-serif;">KENAPA MEMILIH KAMI ?</h1>
          <div class="container">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;padding-top: 50px;padding-bottom: 20px; background-color: #2d85bc;color: #fff;text-align: center;">
                  <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="text-align: left;">
                        <div class="lokasi pross col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                          <div class="col-lg-2" style="margin-bottom: 30px;">
                            <span class="col-lg-12" style="text-align: left;">
                              <i class="fa fa-university wow  rubberBand" aria-hidden="true" style="font-size: 32px;color: #fff;margin-bottom: 5px; animation-duration: 2s;text-align: left;"></i>
                            </span>
                          </div>
                          <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 daft" style="padding-left: 30px;">
                            <p style="text-align: left; color: #fff; font-size: 22px; margin-top: 0px; font-weight: bold;">PERUSAHAAN </p>
                            <p style="text-align: left; margin-top: 25px; font-size: 14px;font-family: 'Roboto', sans-serif;">Perusahaan yang pasti bisa diajak kerja sama dengan Anda.</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="text-align: left;">
                        <div class="lokasi pross col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                          <div class="col-lg-2" style="margin-bottom: 30px;">
                            <span class="col-lg-12" style="text-align: left;">
                              <i class="fa fa-car wow  rubberBand" aria-hidden="true" style="font-size: 32px;color: #fff;margin-bottom: 5px; animation-duration: 2s;text-align: left;"></i>
                            </span>
                          </div>
                          <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 daft" style="padding-left: 30px;">
                            <p style="text-align: left; color: #fff; font-size: 22px; margin-top: 0px; font-weight: bold;">TRANSPORTASI</p>
                            <p style="text-align: left; margin-top: 25px; font-size: 14px;font-family: 'Roboto', sans-serif;">Cari perusahaan yang dekat dengan daerah Anda , sehingga Anda dapat mengukur jarak dari rumah anda.</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="text-align: left;">
                        <div class="lokasi pross col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                          <div class="col-lg-2" style="margin-bottom: 30px;">
                            <span class="col-lg-12" style="text-align: left;">
                              <i class="fa fa-bullhorn wow  rubberBand" aria-hidden="true" style="font-size: 32px;color: #fff;margin-bottom: 5px; animation-duration: 2s;text-align: left;"></i>
                            </span>
                          </div>
                          <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 daft" style="padding-left: 30px;">
                            <p style="text-align: left; color: #fff; font-size: 22px; margin-top: 0px; font-weight: bold;">INFORMASI</p>
                            <p style="text-align: left; margin-top: 25px; font-size: 14px;font-family: 'Roboto', sans-serif;">Informasi yang diberikan secara jelas dan singkat.</p>
                          </div>
                        </div>
                      </div>
                  </div>
                   <div class="row" style="margin-top: 50px;">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="text-align: left;">
                        <div class="lokasi pross col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                          <div class="col-lg-2" style="margin-bottom: 30px;">
                            <span class="col-lg-12" style="text-align: left;">
                              <i class="fa fa-database wow  rubberBand" aria-hidden="true" style="font-size: 32px;color: #fff;margin-bottom: 5px; animation-duration: 2s;text-align: left;"></i>
                            </span>
                          </div>
                          <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 daft " style="padding-left: 30px;">
                            <p style="text-align: left; color: #fff; font-size: 22px; margin-top: 0px; font-weight: bold;">DATABASE</p>
                            <p style="text-align: left; margin-top: 25px; font-size: 14px;font-family: 'Roboto', sans-serif;">Banyak perusahaan yang telah terdaftar dan terdata pada database kami.</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="text-align: left;">
                        <div class="lokasi pross col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                          <div class="col-lg-2" style="margin-bottom: 30px;">
                            <span class="col-lg-12" style="text-align: left;">
                              <i class="fa fa-gamepad wow  rubberBand" aria-hidden="true" style="font-size: 32px;color: #fff;margin-bottom: 5px; animation-duration: 2s;text-align: left;"></i>
                            </span>
                          </div>
                          <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 daft" style="padding-left: 30px;">
                            <p style="text-align: left; color: #fff; font-size: 22px; margin-top: 0px; font-weight: bold;">HOBI</p>
                            <p style="text-align: left; margin-top: 25px; font-size: 14px;font-family: 'Roboto', sans-serif;"> Bahkan Anda dapat mencari pekerjaan sesuai hobi yang anda sukai.</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="text-align: left;">
                        <div class="lokasi pross col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                          <div class="col-lg-2" style="margin-bottom: 30px;">
                            <span class="col-lg-12" style="text-align: left;">
                              <i class="fa fa-users wow  rubberBand" aria-hidden="true" style="font-size: 32px;color: #fff;margin-bottom: 5px; animation-duration: 2s;text-align: left;"></i>
                            </span>
                          </div>
                          <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 daft" style="padding-left: 30px;">
                            <p style="text-align: left; color: #fff; font-size: 22px; margin-top: 0px; font-weight: bold;">SEKELOMPOK TIM</p>
                            <p style="text-align: left; margin-top: 25px; font-size: 14px;font-family: 'Roboto', sans-serif;">Kerjasama pasti dibutuhkan disitus kami, maka dari itu segera cari pekerjaan sesuai minat Anda.</p>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
          </div>
        </div>

      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 0px;padding-top: 45px;padding-bottom: 25px; background-color: #bf3654;color: #fff;text-align: left;">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow jackInTheBox" style="animation-duration: 2s;">
                <h2 style="font-weight: bold;letter-spacing: 1px;text-transform: uppercase; font-family: 'Roboto', sans-serif;">Segera Daftarkan Diri Anda !</h2>
              </div>
            </div>
            
          </div>
          
        </div>

      </div>
    </div>
    <!-- <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn" style="margin-top: 100px; text-align: center;">
          <h1 style="font-weight: bold;color: #000;letter-spacing: 3px;font-family: 'Roboto', sans-serif;"> TEAM KAMI</h1>
          <p style="color: #2f2f30; font-size: 22px;font-family: 'Roboto', sans-serif; margin-top: 20px;">  Kebersamaan adalah kunci dari Keberhasilan Kami </p>
        </div>
      </div>
    </div> -->
    <!-- <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12   col-xs-12" style="margin-top: 70px;">
          <section class="regular slider col-lg-12 col-md-12 col-sm-12   col-xs-12">

              <div style="height: auto; text-align: center;" class="tim">
                <div style="text-align: center;">
                  <img src="http://coderprem.com/amplebiz/images/team/3.jpg">
                  <div class="timm col-lg-2 col-md-2 col-sm-2 col-xs-2 animated zoomIn" style="margin-top: -146px;  padding-top: 10px; padding-bottom: 10px;padding-left: 0px; padding-right: 0px; display: none;">
                    <h3 style="font-weight: normal; color: #000;margin-top: 0px;color:#fff; text-align: left;">
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-facebook-official" style="background-color: #43509e; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-instagram" style="background-color: #b5425d; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-linkedin-square" style="background-color: #061363; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-twitter-square" style="background-color: #959fdb; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                    </h3>
                  </div>
                </div>
                <div style="padding-top: 0px; padding-bottom: 0px; ">
                  <h3 style="font-weight: bold; color: #000;margin-top: 20px;"> Febri Fahturrohman</h3>
                  <p style="font-size: 20px;"> SEO Manager</p>
                </div>
              </div>
              <div style="height: auto; text-align: center;" class="tim">
                <img src="http://coderprem.com/amplebiz/images/team/1.jpg">
                <div class="timm col-lg-2 col-md-2 col-sm-2 col-xs-2 animated zoomIn" style="margin-top: -146px; padding-top: 10px; padding-bottom: 10px;padding-left: 0px; padding-right: 0px; display: none;">
                    <h3 style="font-weight: normal; color: #000;margin-top: 0px;color:#fff; text-align: left;">
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-facebook-official" style="background-color: #43509e; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-instagram" style="background-color: #b5425d; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-linkedin-square" style="background-color: #061363; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-twitter-square" style="background-color: #959fdb; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                    </h3>
                  </div>
                <div style="padding-top: 5px; padding-bottom: 10px;">
                  <h3 style="font-weight: bold; color: #000;margin-top: 20px;"> Dwi Ayu Ningrum</h3>
                <p style="font-size: 20px;"> Front End Development</p>
                </div>
              </div>
              <div style="height: auto; text-align: center;" class="tim">
                <img src="http://coderprem.com/amplebiz/images/team/4.jpg">
                <div class="timm col-lg-2 col-md-2 col-sm-2 col-xs-2 animated zoomIn" style="margin-top: -146px; padding-top: 10px; padding-bottom: 10px;padding-left: 0px; padding-right: 0px; display: none;">
                    <h3 style="font-weight: normal; color: #000;margin-top: 0px;color:#fff; text-align: left;">
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-facebook-official" style="background-color: #43509e; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-instagram" style="background-color: #b5425d; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-linkedin-square" style="background-color: #061363; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-twitter-square" style="background-color: #959fdb; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                    </h3>
                  </div>
                <div style="padding-top: 5px; padding-bottom: 10px;">
                  <h3 style="font-weight: bold; color: #000;margin-top: 20px;"> Aditya Pramudya</h3>
                <p style="font-size: 20px;"> Software Engineering </p>
                </div>
              </div>
              <div style="height: auto; text-align: center;" class="tim">
                <img src="http://coderprem.com/amplebiz/images/team/2.jpg">
                <div class="timm col-lg-2 col-md-2 col-sm-2 col-xs-2 animated zoomIn" style="margin-top: -146px; padding-top: 10px; padding-bottom: 10px;padding-left: 0px; padding-right: 0px; display: none;">
                    <h3 style="font-weight: normal; color: #000;margin-top: 0px;color:#fff; text-align: left;">
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-facebook-official" style="background-color: #43509e; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-instagram" style="background-color: #b5425d; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-linkedin-square" style="background-color: #061363; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-twitter-square" style="background-color: #959fdb; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                    </h3>
                  </div>
                <div style="padding-top: 5px; padding-bottom: 10px;">
                  <h3 style="font-weight: bold; color: #000;margin-top: 20px;"> Shafa Safira P</h3>
                <p style="font-size: 20px;"> Marketing Manager </p>
                </div>
              </div>
              <div style="height: auto; text-align: center;" class="tim">
                <img src="http://coderprem.com/amplebiz/images/team/1.jpg">
               <div class="timm col-lg-2 col-md-2 col-sm-2 col-xs-2 animated zoomIn" style="margin-top: -146px; padding-top: 10px; padding-bottom: 10px;padding-left: 0px; padding-right: 0px; display: none;">
                    <h3 style="font-weight: normal; color: #000;margin-top: 0px;color:#fff; text-align: left;">
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-facebook-official" style="background-color: #43509e; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-instagram" style="background-color: #b5425d; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-linkedin-square" style="background-color: #061363; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                      <a href="#" style="color: #fff;">
                        <i class="fa fa-twitter-square" style="background-color: #959fdb; padding-top: 5px; padding-bottom: 5px;padding-right: 10px;padding-left: 10px;" aria-hidden="true"></i>
                      </a>
                    </h3>
                  </div>
                <div style="padding-top: 5px; padding-bottom: 10px;">
                  <h3 style="font-weight: bold; color: #000;margin-top: 20px;"> Hellena Painem</h3>
                <p style="font-size: 20px;"> Pro Gamer </p>
                </div>
              </div>
          </section>  
        </div>
      </div>
    </div> -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 100px;">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="lok col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-top: 0px;padding-bottom: 20px;margin-top: 0px;">
                <div  class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 wow slideInLeft" style="margin-top: 0px;padding-top: 30px;padding-bottom: 30px;animation-duration: 3s;">
                    <video class="col-lg-12 col-md-12 col-sm-12 col-xs-12" controls>
                        <source src="<?php echo base_url() ?>assets/custom/images/batu.mp4" type="video/mp4" >
                    </video>
                </div>
              </div>
              <div class="lok col-lg-6 col-md-6 col-sm-12 col-xs-12 " style="padding-top: 20px;padding-bottom: 20px;margin-top: 0px;">
                  <div class="deskrip col-lg-12 col-md-12 col-sm-12 col-xs-12 wow pulse" style="padding-top: 0px;padding-bottom: 0px;margin-top: 0px; animation-duration: 4s;">
                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h1 style="font-weight: normal;color: #000; margin-top: 0px;margin-bottom: 40px;font-family: 'Saira Semi Condensed', sans-serif; letter-spacing: 1px;">Kami adalah inovasi global perusahaan</h1>
                        <p style="color: #bf3654; font-size: 22px; margin-top: 15px; font-weight: bold;">DI KOTA BATU</p>
                        <p style="margin-top: 0px;color: #4e4e4f; font-size: 18px;font-family: 'Roboto', sans-serif;">adalah sebuah kota di Provinsi Jawa Timur, Indonesia. Kota ini terletak 90 km sebelah barat daya Surabaya atau 15 km sebelah barat laut Malang. Kota Batu berada di jalur yang menghubungkan Malang-Kediri dan Malang-Jombang. Kota Batu berbatasan dengan Kabupaten Mojokerto dan Kabupaten Pasuruan di sebelah utara serta dengan Kabupaten Malang di sebelah timur, selatan, dan barat. Wilayah kota ini berada di ketinggian 700-1.700 meter di atas permukaan laut dengan suhu udara rata-rata mencapai 12-19 derajat Celsius.</p>
                      </div>
                    </div>
                  </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn subs" style="margin-top: 100px; text-align: center;">
          <div class="container">
              <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 timkami" style="margin-top: 0px; text-align: left;padding-top: 50px;padding-bottom: 50px;">
                  <h1 style="color: #fff;text-shadow: 2px 2px 10px #000; font-family: 'Roboto', sans-serif;"> Berlangganan newsletter kami</h1>
                </div>
                <div class="col-lg-4 col-md-8 col-sm-8 col-xs-12 timkami" style="margin-top: 0px; text-align: left;padding-top: 50px;padding-bottom: 50px;">
                  <h1 style="color: #fff;font-family: 'Roboto', sans-serif;">
                    <div class="input-group input-group-lg" style="margin-left:0px;">
                      <span class="input-group-addon" id="sizing-addon1" style="background-color: #fff;">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                      <input type="text" style="font-family: 'Roboto', sans-serif;font-size: 18px; " class="form-control" placeholder="Masukkan Email" aria-describedby="sizing-addon1">
                      <span class="input-group-btn">
                      <button class="btn btn-Info" type="button" style="padding-top: 10px;padding-bottom: 10px;background-color: #2b3884;">Subscribe</button>
                      </span>
                    </div>
                  </h1>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn working" style="margin-top: 100px; text-align: center;">
          <div class="container-fluid" style="padding-left: 0px !important;padding-right: 0px !important;">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 timkami" style="margin-top: 0px; text-align: left 
              ;padding-top: 100px;padding-bottom: 100px;margin-left: 0px;padding-left: 50px;padding-right: 50px;">
                  <h1 style="color: #000;font-family: 'Lato', sans-serif; font-weight: bold;"> Bekerja Sama</h1>
                  <p style="font-size: 20px;">Tidak hanya melalui kerja sama tim, kolaborasi yang tepat juga dapat membantu Anda untuk membangun perusahaan sendiri atau perusahaan tempat Anda bekerja.</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 timkami" style="margin-top: 0px; text-align: right;padding-top: 250px;padding-bottom: 250px;padding-left: 0px;padding-right: 0px;background-image: url(http://mediad.publicbroadcasting.net/p/wmky/files/201711/house.jpg);background-repeat: no-repeat;background-size: cover;background-position: center;">
                </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn working" style="margin-top: 0px; text-align: center;">
          <div class="container-fluid" style="padding-left: 0px !important;padding-right: 0px !important;">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 timkami" style="margin-top: 0px; text-align: right;padding-top: 250px;padding-bottom: 250px;padding-left: 0px;padding-right: 0px;background-image: url(http://ak1.picdn.net/shutterstock/videos/3872411/thumb/3.jpg); background-repeat: no-repeat;background-size: cover;background-position: center;">
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 timkami" style="margin-top: 0px; text-align: left;padding-top: 100px;padding-bottom: 100px;margin-left: 0px;padding-left: 50px;padding-right: 50px;">
                  <h1 style="color: #000;font-family: 'Lato', sans-serif; font-weight: bold;">Fungsi Kerja Sama</h1>
                  <p style="font-size: 20px;">Bekerja bersama-sama untuk mencapai suatu hasil yang didambakan.Bersama dengan musyawarah, pantun, Pancasila, hukum adat, ketuhanan, dan kekeluargaan.</p>
              </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn working" style="margin-top: 0px; text-align: center;">
          <div class="container-fluid" style="padding-left: 0px !important;padding-right: 0px !important;">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 timkami" style="margin-top: 0px; text-align: left;padding-top: 100px;padding-bottom: 100px;margin-left: 0px;padding-left: 50px;padding-right: 50px;">
                  <h1 style="color: #000;font-family: 'Lato', sans-serif; font-weight: bold;"> Gotong Royong</h1>
                  <p style="font-size: 20px;"> Kami membangun situs ini dengan cara bergotong royong agar menempuh hasil sempurna , Untuk memudahkan Anda mencari pekerjaan.</p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 timkami" style="margin-top: 0px; text-align: right;padding-top: 250px;padding-bottom: 250px;padding-left: 0px;padding-right: 0px;background-image: url(http://mediad.publicbroadcasting.net/p/wmky/files/201711/house.jpg);background-repeat: no-repeat;background-size: cover;background-position: center;">
                </div>
          </div>
        </div>
      </div>  
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn maps" style="margin-top: 100px; text-align: center;">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.8659826610465!2d112.62903231450437!3d-7.909065994302436!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd62a0fdc24144b%3A0x6dde75a43b78ad95!2sKodeku.net!5e0!3m2!1sid!2sid!4v1515732119868" width="100%" height="450px;" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;padding-top: 20px;padding-bottom: 20px;">
          <div class="container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                <h2 style="color: #ccc;font-family: 'Saira Semi Condensed', sans-serif;">Follow Us</h2>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 20px; text-align: center;">
                      <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                      </div>
                      <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;letter-spacing: 10px;">
                        <a href="#" class="sosfoot"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-github" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                      </div>
                      <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                      </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn footeer" style="margin-top: 0px; text-align: center;">
          <div class="container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn footeer" style="margin-top: 0px; text-align: center;padding-bottom: 20px;">
                <h4 style="color: #ccc;"><i class="fa fa-copyright" aria-hidden="true"></i> Copyright. All rights reserved | Designed by Febri Fahturrohman</h4>
              </div>
             
          </div>
        </div>
      </div>
    </div>








<!-- jQuery online menggunakan CDN -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  
<!-- jQuery lokal -->
    <script src="<?php echo base_url() ?>assets/custom/jquery/jquery.min.js"></script>
     <script src="<?php echo base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- Custom JS -->
    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="<?php echo base_url() ?>assets/custom.js"></script>
    <script type="text/javascript">
        var getHeaderHeight = $('.headerContainerWrapper').outerHeight();
        var borderAmount = 2;
        var shadowAmount = 30
        var lastScrollPosition = 0;
        $('.headerContainerWrapper').css('top', '-' + (getHeaderHeight + shadowAmount + borderAmount) + 'px');

        $(window).scroll(function() {
          var currentScrollPosition = $(window).scrollTop();

          if ($(window).scrollTop() > 2 * (getHeaderHeight + shadowAmount + borderAmount) ) {

            $('body').addClass('scrollActive').css('padding-top', getHeaderHeight);
            $('.headerContainerWrapper').css('top', 0);

            if (currentScrollPosition < lastScrollPosition) {
              $('.headerContainerWrapper').css('top', '-' + (getHeaderHeight + shadowAmount + borderAmount) + 'px');
            }
            lastScrollPosition = currentScrollPosition;

          } else {
            $('body').removeClass('scrollActive').css('padding-top', 0);
          }
        });</script>
    <script src="<?php echo base_url() ?>assets/custom/js/wow.min.js"></script>
    <script>
      new WOW().init();
    </script>
    <script src="<?php echo base_url() ?>assets/custom/js/slick.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $('.regular').slick({
            dots: true,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,         
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            dots: true,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            dots: true,
            infinite: true,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true,
            infinite: true,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
          ]
          });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript">
$(window).load(function() {
    $(".loader").fadeOut("slow");
});
</script>
   </body>
 </html>