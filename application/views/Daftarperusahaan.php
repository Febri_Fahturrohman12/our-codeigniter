 <html lang="en">
   <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>LOKER</title>
 
     <!-- Bootstrap -->
     <link href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
 
     <!-- HTML5 shim and Respond.js for IE8 support of 
          HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
 
    <!-- Custom CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/custom/css/font-awesome.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/custom/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash|Courgette|Quicksand:300|Saira+Semi+Condensed" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/custom/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/custom/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/custom/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/custom/css/slick-theme.css">
    <style type="text/css">
        body{
          background: #FCFFF9 !important;
        }
        html, body {
          margin: 0;
          padding: 0;
        }

        * {
          box-sizing: border-box;
        }
        .navbar-inverse{
          background:transparent;
          background-image:none;
          border-color:transparent;
          max-height: 80px;
          position:  absolute;
          width: 100%;
          height: auto;
          border-radius: 0px;
          left: 0;
          right: 0;
          top: 20 ;
          width: 100%;
          border: solid 2px #e1eaea;
        }
        .fileUpload {
          position: relative;
          overflow: hidden;
          margin: 10px;
        }
        .fileUpload input.upload {
          position: absolute;
          top: 0;
          right: 0;
          margin: 0;
          padding: 0;
          font-size: 20px;
          cursor: pointer;
          opacity: 0;
          filter: alpha(opacity=0);
      }
    </style>
    <link href="<?php echo base_url() ?>assets/custom/css/style.css" rel="stylesheet">
 
   </head>
   <body>
    <div class="loader"></div>
    <div class="container-fluid col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 100px;">
                <div class="container">
                    <span style="font-size: 12px;color: #8d8d8d">Home > Daftar</span>
                </div>
                
                <nav class="navbar navbar-inverse container-fluid">
                  <div class="container">
                    <div class="navbar-header col-lg-10 col-md-10 col-sm-10 col-xs-12" style="text-align: left;">  
                      <a class="navbar-brand" style="color: #000;font-weight: bold;font-family: 'Saira Semi Condensed', sans-serif;" href="<?php echo site_url('pertama/')?>"><span class="glyphicon glyphicon-send" aria-hidden="true"></span> Lowongan Kerja</a>
                    </div>
                  </div>
                </nav>
    </div>
    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6">
        <div class="header">

          <div class="header__text-group text-center">
            <h1 class="header__heading">Lowongan Pekerjaan</h1>
            <p class="header__subheading">Buat Akun Perusahaan</p>
          </div> <!-- close .text-group -->
        </div> <!-- close .header -->
    
        <hr />
        <div class="form-group text-center">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style=";padding-top: 20px;padding-bottom: 20px;text-align: left;">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
                <img src="https://id-live.slatic.net/cms/banners2017/052017/contact-chat.png">
              </div>
          </div>
        </div>
      <?php echo form_open("login/daftarperusahaan", array('enctype'=>'multipart/form-data')); ?>
        



        <div class="form-register">
        <div class="row">
          <div class="col-xs-6">
            <div class="form-group">
              <label for="user_first_name">Nama Perusahaan</label>
              <input class="form-control" required="required" type="text" name="perusahaan_nama">
            </div> <!-- close .form-group -->
          </div> <!-- close .col -->

          <div class="col-xs-6">
            <div class="form-group">
              <label for="user_last_name">Nomor Telepon</label>
              <input class="form-control" type="number" name="perusahaan_nopon" required="required">
            </div> <!-- close .form-group -->
          </div> <!-- close .col -->
        </div> <!-- close .row -->

        <div class="form-group">
          <label for="user_email">
            Alamat Email
          </label>
          <input class="form-control" type="email" value="" required="required" name="perusahaan_email">

        </div> <!-- close .form-group -->

        <div class="form-group">
          <label for="user_email">
            Alamat Website
          </label>
          <input class="form-control" type="text" value=""  name="perusahaan_website">

        </div> <!-- close .form-group -->

        <div class="form-group">
          <label for="comment">Alamat Kantor</label>
          <textarea class="form-control" rows="5" name="perusahaan_alamat" required="required" style="resize: none;"></textarea>
        </div>

        <div class="form-group  file-upload is-empty is-fileinput">
         <input id="uploadFile" style="width: 50%;background-color: #fff;box-shadow: none;border:none;border-bottom: 1px solid #ccc;" placeholder="*Kirim Logo dengan format jpg atau png. Ukuran file maksimal 5MB" disabled="disabled" required="required" name="perusahaan_logo" />
          <div class="fileUpload btn btn-primary">
            <span><i class="fa fa-paperclip" aria-hidden="true"></i></span>
            <input id="uploadBtn" type="file" required="required" class="upload" name="input_gambar" />
          </div>
          <div style="color: red;font-weight: bold;"><?php echo (isset($message))? $message : ""; ?></div>
        </div>

        <div class="form-group">
          <label for="user_username">
            Username
          </label>
          <input class="form-control" required="required" type="text" name="perusahaan_username">
        </div> <!-- close .form-group -->

        <div class="form-group">
          <label for="user_password">
            Password <span class="text-secondary">(min. 6 char)</span>
          </label>
          <input class="form-control" required="required" type="password" name="perusahaan_password">

        </div> <!-- close .form-group -->

        <div class="form-group">
          <label for="comment">Tentang Perusahaan</label>
          <textarea class="form-control" rows="5" name="perusahaan_tentang" required="required" style="resize: none;"></textarea>
        </div>

        <div class="form-group">
          <label for="user_subscribe_to_newsletter">
            <input name="user[subscribe_to_newsletter]" type="hidden" value="0">
            <input type="checkbox" value="1" name="user[subscribe_to_newsletter]"  id="user_subscribe_to_newsletter">
            Terima newsleter
          </label>
        </div> <!-- close .form-group -->

        <div class="form-group">
          <input type="submit" value="Buat Akun" name="submit" class="btn btn-primary">
        </div> <!-- close .form-group -->

        <div class="form-group text-center">
          <p class="text-secondary zeta">
            Dengan bergabung, Anda menyetujui <a href="/terms">Persyaratan dan Kebijakan Privasi.</a>.
          </p>
        </div>
</div> <!-- close .form -->
      <?php echo form_close(); ?>

      <div class="sheet sheet--padding-small text-center">
        <p class="epsilon">Sudah Memiliki Akun? <a href="<?php echo site_url('pertama/masuk')?>">Login</a></p>
      </div>
    </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;padding-top: 20px;padding-bottom: 20px;">
          <div class="container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                <h2 style="color: #ccc;font-family: 'Saira Semi Condensed', sans-serif;">Follow Us</h2>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 20px; text-align: center;">
                      <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                      </div>
                      <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;letter-spacing: 10px;">
                        <a href="#" class="sosfoot"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-github" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                      </div>
                      <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                      </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn footeer" style="margin-top: 0px; text-align: center;">
          <div class="container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn footeer" style="margin-top: 0px; text-align: center;padding-bottom: 20px;">
                <h4 style="color: #ccc;"><i class="fa fa-copyright" aria-hidden="true"></i> Copyright. All rights reserved | Designed by Febri Fahturrohman</h4>
              </div>
             
          </div>
        </div>
      </div>
    </div>








<!-- jQuery online menggunakan CDN -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  
<!-- jQuery lokal -->
    <script src="<?php echo base_url() ?>assets/custom/jquery/jquery.min.js"></script>
     <script src="<?php echo base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- Custom JS -->
    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="<?php echo base_url() ?>assets/custom.js"></script>
    <script type="text/javascript">
        var getHeaderHeight = $('.headerContainerWrapper').outerHeight();
        var borderAmount = 2;
        var shadowAmount = 30
        var lastScrollPosition = 0;
        $('.headerContainerWrapper').css('top', '-' + (getHeaderHeight + shadowAmount + borderAmount) + 'px');

        $(window).scroll(function() {
          var currentScrollPosition = $(window).scrollTop();

          if ($(window).scrollTop() > 2 * (getHeaderHeight + shadowAmount + borderAmount) ) {

            $('body').addClass('scrollActive').css('padding-top', getHeaderHeight);
            $('.headerContainerWrapper').css('top', 0);

            if (currentScrollPosition < lastScrollPosition) {
              $('.headerContainerWrapper').css('top', '-' + (getHeaderHeight + shadowAmount + borderAmount) + 'px');
            }
            lastScrollPosition = currentScrollPosition;

          } else {
            $('body').removeClass('scrollActive').css('padding-top', 0);
          }
        });</script>
    <script src="<?php echo base_url() ?>assets/custom/js/wow.min.js"></script>
    <script>
      new WOW().init();
    </script>
    <script src="<?php echo base_url() ?>assets/custom/js/slick.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $('.regular').slick({
            dots: true,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,         
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            dots: true,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            dots: true,
            infinite: true,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true,
            infinite: true,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
          ]
          });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript">
      $(window).load(function() {
          $(".loader").fadeOut("slow");
      });
    </script>
    <script type="text/javascript">
      jQuery(document).ready(function ($) {
        $('#tabs').tab();
      });
    </script>
    <script>
      document.getElementById("uploadBtn").onchange = function () {
      document.getElementById("uploadFile").value = this.value;
      };
    </script>
   </body>
 </html>