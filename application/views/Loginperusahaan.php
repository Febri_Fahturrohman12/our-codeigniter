 <html lang="en">
   <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>LOKER</title>
 
     <!-- Bootstrap -->
     <link href="<?php echo base_url() ?>assets/perusahaan/bootstrap/css/bootstrap.min.css" rel="stylesheet">
 
     <!-- HTML5 shim and Respond.js for IE8 support of 
          HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
 
    <!-- Custom CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/perusahaan/custom/css/font-awesome.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/perusahaan/custom/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash|Courgette|Quicksand:300|Saira+Semi+Condensed" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/perusahaan/custom/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/perusahaan/custom/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/perusahaan/custom/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/perusahaan/custom/css/slick-theme.css">
    <style type="text/css">
        body{
          background: #FCFFF9 !important;
        }
        html, body {
          margin: 0;
          padding: 0;
        }

        * {
          box-sizing: border-box;
        }
        .navbar-inverse{
          background:transparent;
          background-image:none;
          border-color:transparent;
          max-height: 80px;
          position:  absolute;
          width: 100%;
          height: auto;
          border-radius: 0px;
          left: 0;
          right: 0;
          top: 20 ;
          width: 100%;
          border: solid 2px #e1eaea;
        }
        
/* Login Form */
.login-container {
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;
  width: 340px;
  height: auto;
  padding: 5px;
  box-sizing: border-box;
}

.login-container img {
  width: 200px;
  margin: 0 0 20px 0;
}

.login-container p {
  align-self: flex-start;
  font-family: 'Roboto', sans-serif;
  font-size: 0.8rem;
  color: rgba(0, 0, 0, 0.5);
}

.login-container p a {
  color: rgba(0, 0, 0, 0.4);
}

.login {
  position: relative;
  padding: 10px;
  margin: 0 0 10px 0;
  box-sizing: border-box;
  border-radius: 3px;
  background: #FAFAFA;
  overflow: hidden;
  animation: input_opacity 0.2s cubic-bezier(.55, 0, .1, 1);
  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14),
              0 1px 5px 0 rgba(0, 0, 0, 0.12),
              0 3px 1px -2px rgba(0, 0, 0, 0.2);
}

.login > header {
  position: relative;
  padding: 10px;
  margin: -10px -10px 25px -10px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  background: #009688;
  font-family: 'Roboto', sans-serif;
  font-size: 1.3rem;
  color: #FAFAFA;
  animation: scale_header 0.6s cubic-bezier(.55, 0, .1, 1), text_opacity 1s cubic-bezier(.55, 0, .1, 1);
  box-shadow: 0px 2px 2px 0px rgba(0, 0, 0, 0.14),
              0px 1px 5px 0px rgba(0, 0, 0, 0.12),
              0px 3px 1px -2px rgba(0, 0, 0, 0.2);
}

.login > header:before {
  content: '';
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  width: 100%;
  height: 5px;
  padding: 10px;
  margin: -10px 0 0 -10px;
  box-sizing: border-box;
  background: rgba(0, 0, 0, 0.156);
  font-family: 'Roboto', sans-serif;
  font-size: 0.9rem;
  color: transparent;
  z-index: 5;
}

.login.error_1 > header:before,
.login.error_2 > header:before {
  animation: error_before 3s cubic-bezier(.55, 0, .1, 1);
}

.login.error_1 > header:before {
  content: 'Invalid username or password!';
}

.login.error_2 > header:before {
  content: 'Invalid or expired Token!';
}

.login > header h2 {
  margin: 50px 0 10px 0;
}

.login > header h4 {
  
  animation: text_opacity 1.5s cubic-bezier(.55, 0, .1, 1);
  color: rgba(255, 255, 255, 0.4);
}

/* Form */
.login-form {
  padding: 15px;
  box-sizing: border-box;
}


/* Inputs */
.login-input {
  position: relative;
  width: 100%;
  padding: 10px 5px;
  margin: 0 0 25px 0;
  border: none;
  border-bottom: 2px solid rgba(0, 0, 0, 0.2);
  box-sizing: border-box;
  background: transparent;
  font-family: 'Roboto', sans-serif;
  font-weight: 500;
  opacity: 1;
  animation: input_opacity 0.8s cubic-bezier(.55, 0, .1, 1);
  transition: border-bottom 0.2s cubic-bezier(.55, 0, .1, 1);
}

.login-input:focus {
  outline: none;
  border-bottom: 2px solid #E37F00;
}


/* Submit Button */
.submit-container {
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  position: relative;
  padding: 10px;
  margin: 35px -25px -25px -25px;
  border-top: 1px solid rgba(0, 0, 0, 0.1);
}

.login-button {
  padding: 10px;
  border: none;
  border-radius: 3px;
  background: transparent;
  font-family: 'Roboto', sans-serif;
  font-size: 1.9rem;
  font-weight: 500;
  color: #E37F00;
  cursor: pointer;
  opacity: 1;
  animation: input_opacity 0.8s cubic-bezier(.55, 0, .1, 1);
  transition: background 0.2s ease-in-out;
}

.login-button.raised {
  padding: 5px 10px;
  color: #FAFAFA;
  background: #E37F00;
  box-shadow: 0px 2px 2px 0px rgba(0, 0, 0, 0.137255),
              0px 1px 5px 0px rgba(0, 0, 0, 0.117647),
              0px 3px 1px -2px rgba(0, 0, 0, 0.2);
}

.login-button:hover {
  background: rgba(0, 0, 0, 0.05);
}

.login-button.raised:hover {
  background: #FDAB43;
}

    </style>
    <link href="<?php echo base_url() ?>assets/perusahaan/custom/css/style.css" rel="stylesheet">
 
   </head>
   <body>
    <div class="loader"></div>
    <div class="container-fluid col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 100px;">
      <div class="container">
        <span style="font-size: 12px;color: #8d8d8d">Home > Login</span>
      </div>
      <nav class="navbar navbar-inverse container-fluid">
        <div class="container">
            <div class="navbar-header col-lg-10 col-md-10 col-sm-10 col-xs-12" style="text-align: left;">  
              <a class="navbar-brand" style="color: #000;font-weight: bold;font-family: 'Saira Semi Condensed', sans-serif;" href="index.php"><span class="glyphicon glyphicon-send" aria-hidden="true"></span> Lowongan Kerja</a>
            </div>
        </div>
      </nav>
    </div>
    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;margin-bottom: 100px;">
      <div class="row">
        <div class="container col-lg-4 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
        </div>
        <div class="container col-lg-4 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
          <div class="login-container">
        <section class="login" id="login">
          <header>
            <h2>Sebagai Perusahaan</h2>
            <h4>Masuk</h4>
          </header>
          <form class="login-form" action="<?php echo base_url('index.php/login/aksi_loginp'); ?>" method="post"> 
            <input type="text" class="login-input" placeholder="Username" name="perusahaan_username" required autofocus/>
            <input type="password" class="login-input" placeholder="Password" name="perusahaan_password" required/>
            <div class="submit-container">
              <button type="submit" name="submit" class="login-button">Masuk</button>
            </div>
          </form>
        </section>
          </div>
        </div>
        <div class="container col-lg-4 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
        </div>
      </div>
    </div>





    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;padding-top: 20px;padding-bottom: 20px;">
          <div class="container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                <h2 style="color: #ccc;font-family: 'Saira Semi Condensed', sans-serif;">Follow Us</h2>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 20px; text-align: center;">
                      <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                      </div>
                      <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;letter-spacing: 10px;">
                        <a href="#" class="sosfoot"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-github" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                      </div>
                      <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                      </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn footeer" style="margin-top: 0px; text-align: center;">
          <div class="container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn footeer" style="margin-top: 0px; text-align: center;padding-bottom: 20px;">
                <h4 style="color: #ccc;"><i class="fa fa-copyright" aria-hidden="true"></i> Copyright. All rights reserved | Designed by Febri Fahturrohman</h4>
              </div>
             
          </div>
        </div>
      </div>
    </div>








<!-- jQuery online menggunakan CDN -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  
<!-- jQuery lokal -->
    <script src="<?php echo base_url() ?>assets/perusahaan/custom/jquery/jquery.min.js"></script>
     <script src="<?php echo base_url() ?>assets/perusahaan/bootstrap/js/bootstrap.min.js"></script>
    <!-- Custom JS -->
    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="<?php echo base_url() ?>assets/perusahaan/custom.js"></script>
    <script type="text/javascript">
        var getHeaderHeight = $('.headerContainerWrapper').outerHeight();
        var borderAmount = 2;
        var shadowAmount = 30
        var lastScrollPosition = 0;
        $('.headerContainerWrapper').css('top', '-' + (getHeaderHeight + shadowAmount + borderAmount) + 'px');

        $(window).scroll(function() {
          var currentScrollPosition = $(window).scrollTop();

          if ($(window).scrollTop() > 2 * (getHeaderHeight + shadowAmount + borderAmount) ) {

            $('body').addClass('scrollActive').css('padding-top', getHeaderHeight);
            $('.headerContainerWrapper').css('top', 0);

            if (currentScrollPosition < lastScrollPosition) {
              $('.headerContainerWrapper').css('top', '-' + (getHeaderHeight + shadowAmount + borderAmount) + 'px');
            }
            lastScrollPosition = currentScrollPosition;

          } else {
            $('body').removeClass('scrollActive').css('padding-top', 0);
          }
        });</script>
    <script src="assets/perusahaan/custom/js/wow.min.js"></script>
    <script>
      new WOW().init();
    </script>
    <script src="<?php echo base_url() ?>assets/perusahaan/custom/js/slick.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $('.regular').slick({
            dots: true,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,         
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            dots: true,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            dots: true,
            infinite: true,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true,
            infinite: true,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
          ]
          });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript">
      $(window).load(function() {
          $(".loader").fadeOut("slow");
      });
    </script>
    <script type="text/javascript">
      jQuery(document).ready(function ($) {
        $('#tabs').tab();
      });
    </script>
   </body>
 </html>