 <html lang="en">
   <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>LOKER</title>
 
     <!-- Bootstrap -->
     <link href="<?php echo base_url() ?>assets/perusahaan/bootstrap/css/bootstrap.min.css" rel="stylesheet">
 
     <!-- HTML5 shim and Respond.js for IE8 support of 
          HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
 
    <!-- Custom CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/perusahaan/custom/css/font-awesome.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/perusahaan/custom/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash|Courgette|Quicksand:300|Saira+Semi+Condensed" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/perusahaan/custom/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/perusahaan/custom/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/perusahaan/custom/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/perusahaan/custom/css/slick-theme.css">
    <style type="text/css">
        html, body {
          margin: 0;
          padding: 0;
        }

        * {
          box-sizing: border-box;
        }

        .slider {
            width: 100%;
            margin: 0px auto;
        }

        .slick-slide {
          margin: 0px 20px;
        }

        .slick-slide img {
          width: 100%;
          background-size: cover;
        }

        .slick-prev:before,
        .slick-next:before {
          color: black;
        }


        .slick-slide {
          transition: all ease-in-out .3s;
          opacity: .2;
        }
        
        .slick-active {
          opacity: 1;
        }

        .slick-current {
          opacity: 1;
        }
        .special-img 
        {
          position: relative;
          top: -10px;
          float: left;
          left: -5px;
          width: 40px;
          height: 40px;
          margin-right: 5px;
        }
        .avataropsi{
          margin-top: 10px !important;
          padding-left:10px;
          padding-right: 10px;
        }
    </style>
    <link href="<?php echo base_url() ?>assets/perusahaan/custom/css/style.css" rel="stylesheet">
 
   </head>
   <body>
    <div class="loader"></div>
    <div class="container-fluid" style="margin: 0px;padding: 0px;">
      <!-- Bagian Header -->
      <div class="abp col-lg-12 col-md-12 col-sm-12 col-xs-12" id="site-header" >
          <header>
            <div class="col-lg-12 col-md-12 col-xs-12 sosmed header" id="site-header" style="height: 40px; max-height: 40px;background-color: rgba(0,0,0,0.26);">
              <div class="col-lg-12 col-xs-12 col-md-12">
                <ul class="sosmed nav navbar-nav navbar-right">
                  <li><a href="#" class="sos"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="sos"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="sos"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="sos"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                </ul>
              </div>
            </div>
            <nav class="navbar navbar-default col-lg-12 headerContainerWrapper" style="background-color: rgba(0,0,0,0.26); padding-top: 10px;padding-bottom: 10px; border: none; border-top: 0.1px solid #848484;  border-radius: 0px;">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" style="color: #fff;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>

                  <a class="navbar-brand" style="color: #fff;font-weight: bold;font-family: 'Saira Semi Condensed', sans-serif;" href="<?php echo site_url('perusahaan/')?>"><span class="glyphicon glyphicon-send" aria-hidden="true"></span> Lowongan Kerja</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo site_url('perusahaan/tentang')?>">Tentang Kami</a></li>
                    <li><a href="<?php echo site_url('perusahaan/pekerjaan')?>">Pekerjaan <span class="sr-only">(current)</span></a></li>
                    <li><a href="<?php echo site_url('perusahaan/pelamar')?>">Pelamar</a></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle profile-image" data-toggle="dropdown">
                        <img src="<?php echo base_url('assets/logoperusahaan/'.$this->session->userdata('perusahaan_logo')) ?>" class="img-circle special-img">
                         <?php echo " ".strtoupper($this->session->userdata('perusahaan_nama')); ?></a>
                        <ul class="dropdown-menu avataropsi">
                          <li><a href="<?php echo site_url('perusahaan/akun')?>"><i class="fa fa-cog"></i> Pengaturan Akun</a></li>
                          <li class="divider"></li>
                          <li><a href="<?php echo base_url('login/logout'); ?>"><i class="fa fa-sign-out"></i> Keluar</a></li>
                        </ul>
                    </li>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>
          </header>
          <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12 subhead" style="padding-top: 20px; padding-left: 50px;display: inline-grid;">
                            <h1 style="color: #fff; font-weight: bold;font-size: 42px; text-shadow: 1px 1px 3px #000;">Pelamar</h1>
                            <p style="color: #ddd; font-weight: normal;font-size: 20px; text-shadow: 1px 1px 3px #000;">Website Resmi Tentang Lowongan Pekerjaan Sekitar Kota Batu</p>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 subhead" style="padding-top: 20px; padding-left: 50px; padding-bottom: 40px;">
            <h3 style="font-style: normal;font-weight: bold; font-size: 20px; color: #fff; padding-bottom: 50px;"></h3>
          </div>
    </div>
    <div class="container-fluid perusa" style="padding-bottom: 0px;padding-top: 100px; margin-top: 100px;">
      <div class="row">
        <div class="container">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami " style="margin-top: 100px; text-align: left;overflow: hidden;display: block;margin-bottom: 5px;background-color: #fff; border: 1px solid #eeeeee;box-shadow: 0 1px 2px 0 rgba(0,0,0,0.05);transition: box-shadow .5s, background-color 0.5s;">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="background-color: #fff;" style="text-align: left;padding-top: 20px;padding-bottom: 10px;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-bottom: 0.25px solid #e8e8e8;">
                  <nav aria-label="Page navigation">
                    <ul class="pagination">
                      <li>
                        <a href="#" aria-label="Previous">
                          <span aria-hidden="true">&laquo;</span>
                        </a>
                      </li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                      <li>
                        <a href="#" aria-label="Next">
                          <span aria-hidden="true">&raquo;</span>
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="background-color: #fff;text-align: right;padding-top: 20px;padding-bottom: 10px;">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn" style="">
                      <button class="btn btn-info" type="button" style="font-weight: bold;">Cari</button>
                    </span>
                </div><!-- /input-group -->
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid perusa" style="padding-bottom: 50px;padding-top: 10px;">
      <div class="row">
        <div class="container">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami lowpeker" style="margin-top: 0px; text-align: left;overflow: hidden;display: block;margin-bottom: 5px;background-color: #fff; border: 1px solid #eeeeee;box-shadow: 0 1px 2px 0 rgba(0,0,0,0.05);transition: box-shadow .5s, background-color 0.5s;">
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="background-color: #fff;text-align: center;padding-top: 10px;padding-bottom: 10px;">
                <img src="<?php echo base_url() ?>assets/perusahaan/custom/images/person-flat.png" style="width: 100px; height: 100px;">
              </div>
              <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style="background-color: #fff;" style="text-align: left;padding-top: 20px;padding-bottom: 10px;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-bottom: 0.25px solid #e8e8e8;">
                  <a href="<?php echo site_url('perusahaan/profilpelamar')?>" style="text-decoration: none;font-family: 'Saira Semi Condensed', sans-serif;"><h2 style="color: #bf3654;letter-spacing: 0px;">Febri Fahturrohman</h2></a> 
                  <h5 style="color: #000d21;"><i class="fa fa-suitcase" style="color: rgb(243, 169, 19);" aria-hidden="true"></i> &nbsp Lowongan Pengantar Surat</h5>
                </div>
              </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami lowpeker " style="margin-top: 0px; text-align: left;overflow: hidden;display: block;margin-bottom: 5px;background-color: #fff; border: 1px solid #eeeeee;box-shadow: 0 1px 2px 0 rgba(0,0,0,0.05);transition: box-shadow .5s, background-color 0.5s;">
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="background-color: #fff;text-align: center;padding-top: 10px;padding-bottom: 10px;">
                <img src="<?php echo base_url() ?>assets/perusahaan/custom/images/person-flat.png" style="width: 100px; height: 100px;">
              </div>
              <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style="background-color: #fff;" style="text-align: left;padding-top: 20px;padding-bottom: 10px;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-bottom: 0.25px solid #e8e8e8;">
                  <a href="<?php echo site_url('perusahaan/profilpelamar')?>" style="text-decoration: none;font-family: 'Saira Semi Condensed', sans-serif;"><h2 style="color: #bf3654;letter-spacing: 0px;">Aditya Pramudya Firdaus</h2></a> 
                  <h5 style="color: #000d21;"><i class="fa fa-suitcase" style="color: rgb(243, 169, 19);" aria-hidden="true"></i> &nbsp Lowongan Pengantar Surat</h5>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;padding-top: 20px;padding-bottom: 20px;">
          <div class="container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                <h2 style="color: #ccc;font-family: 'Saira Semi Condensed', sans-serif;">Follow Us</h2>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 20px; text-align: center;">
                      <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                      </div>
                      <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;letter-spacing: 10px;">
                        <a href="#" class="sosfoot"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-github" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                      </div>
                      <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                      </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn footeer" style="margin-top: 0px; text-align: center;">
          <div class="container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn footeer" style="margin-top: 0px; text-align: center;padding-bottom: 20px;">
                <h4 style="color: #ccc;"><i class="fa fa-copyright" aria-hidden="true"></i> Copyright. All rights reserved | Designed by Febri Fahturrohman</h4>
              </div>
             
          </div>
        </div>
      </div>
    </div>








<!-- jQuery online menggunakan CDN -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  
<!-- jQuery lokal -->
    <script src="<?php echo base_url() ?>assets/perusahaan/custom/jquery/jquery.min.js"></script>
     <script src="<?php echo base_url() ?>assets/perusahaan/bootstrap/js/bootstrap.min.js"></script>
    <!-- Custom JS -->
    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="<?php echo base_url() ?>assets/perusahaan/custom.js"></script>
    <script type="text/javascript">
        var getHeaderHeight = $('.headerContainerWrapper').outerHeight();
        var borderAmount = 2;
        var shadowAmount = 30
        var lastScrollPosition = 0;
        $('.headerContainerWrapper').css('top', '-' + (getHeaderHeight + shadowAmount + borderAmount) + 'px');

        $(window).scroll(function() {
          var currentScrollPosition = $(window).scrollTop();

          if ($(window).scrollTop() > 2 * (getHeaderHeight + shadowAmount + borderAmount) ) {

            $('body').addClass('scrollActive').css('padding-top', getHeaderHeight);
            $('.headerContainerWrapper').css('top', 0);

            if (currentScrollPosition < lastScrollPosition) {
              $('.headerContainerWrapper').css('top', '-' + (getHeaderHeight + shadowAmount + borderAmount) + 'px');
            }
            lastScrollPosition = currentScrollPosition;

          } else {
            $('body').removeClass('scrollActive').css('padding-top', 0);
          }
        });</script>
    <script src="<?php echo base_url() ?>assets/perusahaan/custom/js/wow.min.js"></script>
    <script>
      new WOW().init();
    </script>
    <script src="<?php echo base_url() ?>assets/perusahaan/custom/js/slick.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $('.regular').slick({
            dots: true,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,         
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            dots: true,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            dots: true,
            infinite: true,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true,
            infinite: true,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
          ]
          });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript">
$(window).load(function() {
    $(".loader").fadeOut("slow");
});
</script>
   </body>
 </html>