 <html lang="en">
   <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>LOKER</title>
 
     <!-- Bootstrap -->
     <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
 
     <!-- HTML5 shim and Respond.js for IE8 support of 
          HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
 
    <!-- Custom CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" href="assets/custom/css/font-awesome.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/custom/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash|Courgette|Quicksand:300|Saira+Semi+Condensed" rel="stylesheet">
    <link rel="stylesheet" href="assets/custom/css/animate.css">
    <link rel="stylesheet" href="assets/custom/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="assets/custom/css/slick.css">
    <link rel="stylesheet" type="text/css" href="assets/custom/css/slick-theme.css">
    <style type="text/css">
        html, body {
          margin: 0;
          padding: 0;
        }

        * {
          box-sizing: border-box;
        }

        .slider {
            width: 100%;
            margin: 0px auto;
        }

        .slick-slide {
          margin: 0px 20px;
        }

        .slick-slide img {
          width: 100%;
          background-size: cover;
        }

        .slick-prev:before,
        .slick-next:before {
          color: black;
        }


        .slick-slide {
          transition: all ease-in-out .3s;
          opacity: .2;
        }
        
        .slick-active {
          opacity: 1;
        }

        .slick-current {
          opacity: 1;
        }
        .special-img 
        {
          position: relative;
          top: -10px;
          float: left;
          left: -5px;
          width: 40px;
          height: 40px;
          margin-right: 5px;
        }
        .avataropsi{
          margin-top: 10px !important;
          padding-left:10px;
          padding-right: 10px;
        }
    </style>
    <link href="assets/custom/css/style.css" rel="stylesheet">
 
   </head>
   <body>
    <div class="loader"></div>
    <div class="container-fluid" style="margin: 0px;padding: 0px;">
      <!-- Bagian Header -->
      <div class="perus col-lg-12 col-md-12 col-sm-12 col-xs-12" id="site-header" >
          <header>
            <div class="col-lg-12 col-md-12 col-xs-12 sosmed header" id="site-header" style="height: 40px; max-height: 40px;background-color: rgba(0,0,0,0.26);">
              <div class="col-lg-12 col-xs-12 col-md-12">
                <ul class="sosmed nav navbar-nav navbar-right">
                  <li><a href="#" class="sos"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="sos"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="sos"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="sos"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                </ul>
              </div>
            </div>
            <nav class="navbar navbar-default col-lg-12 headerContainerWrapper" style="background-color: rgba(0,0,0,0.26); padding-top: 10px;padding-bottom: 10px; border: none; border-top: 0.1px solid #848484;  border-radius: 0px;">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" style="color: #fff;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>

                  <a class="navbar-brand" style="color: #fff;font-weight: bold;font-family: 'Saira Semi Condensed', sans-serif;" href="index.php"><span class="glyphicon glyphicon-send" aria-hidden="true"></span> Lowongan Kerja</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="Pekerjaan.php">Pekerjaan <span class="sr-only">(current)</span></a></li>
                    <li><a href="Perusahaan.php">Perusahaan</a></li>
                    <li><a href="Tentang.php">Tentang Kami</a></li>
                    <li><a href="Kontak.php">Kontak</a></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle profile-image" data-toggle="dropdown">
                        <img src="assets/custom/images/person-flat.png" class="img-circle special-img"> Febri Fahturrohman</a>
                        <ul class="dropdown-menu avataropsi">
                          <li><a href="Akun.php"><i class="fa fa-cog"></i> Pengaturan Akun</a></li>
                          <li class="divider"></li>
                          <li><a href="#"><i class="fa fa-sign-out"></i> Keluar</a></li>
                        </ul>
                    </li>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>
          </header>
          <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12 subhead" style="padding-top: 40px; padding-left: 50px;display: inline-grid;padding-bottom: 150px;">
                            <h1 style="color: #fff; font-weight: bold;font-size: 42px; text-shadow: 1px 1px 3px #000;"></h1>
          </div>
    </div>
    
      
    <div class="container">

      <div class="row">
        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -200px;padding-bottom: 50px; padding-left: 40px;padding-right: 40px;border: 1px solid #b0b3b7;background-color: #fff;">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #fff;">
              <div class="lokasi col-lg-10 col-md-10 col-sm-10 col-xs-12" style="padding-top: 20px;padding-bottom: 0px;margin-top: 0px;text-align: left;">
                <h1 style="color: #bf3654;margin-top: 15px; font-weight: bold;">
                  PT.Persero Indonesia</h1>
              </div>
              <div class="lokasi col-lg-2 col-md-2 col-sm-2 col-xs-12" style="padding-top: 20px;padding-bottom: 0px;margin-top: 0px;text-align: left;">
              <img src="http://www.posindonesia.co.id/wp-content/uploads/2016/11/Logo-Pos-Indonesia-2012.jpg" style="width: 100px; height: 100px;">
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #fff;">
            <div class="lokasi col-md-12 col-sm-12 col-xs-12" style="padding-top: 0px;padding-bottom: 20px;margin-top: 0px;text-align: left;">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style=";text-align: left;">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 2px;padding-bottom: 2px;">
                    <h5 style="font-family: 'Saira Semi Condensed' sans-serif;color: #505151;font-weight: bold;"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp Jl.Hassanudin RT 02/ RW 06 , Desa Junrejo , Kecamatan Junrejo.</h5>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 2px;padding-bottom: 2px;">
                    <h5 style="font-family: 'Saira Semi Condensed' sans-serif;color: #505151;font-weight: bold;"><i class="fa fa-link" aria-hidden="true"></i>&nbsp http://pt-persero.com</h5>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 2px;padding-bottom: 2px;">
                    <h5 style="font-family: 'Saira Semi Condensed' sans-serif;color: #505151;font-weight: bold;"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp +62 85812337677</h5>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style=";text-align: left;">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 2px;padding-bottom: 2px;">
                    <h5 style="font-family: 'Saira Semi Condensed' sans-serif;color: #505151;font-weight: bold;">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.</h5>
                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4" style="padding-top: 2px;padding-bottom: 2px;">
                    <div style="border:none;width:40px;height:40px;border-radius:40px;background:#ddd;text-align: center;padding-top: 3px;padding-bottom: 5px;color: blue;">
                      <a href=""><h4><i class="fa fa-facebook" aria-hidden="true"></i></h4></a>
                    </div>
                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4" style="padding-top: 2px;padding-bottom: 2px;">
                    <div style="border:none;width:40px;height:40px;border-radius:40px;background:#ddd;text-align: center;padding-top: 3px;padding-bottom: 5px;color: blue;">
                      <a href=""><h4><i class="fa fa-twitter" aria-hidden="true"></i></h4></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
        
      </div> 
    </div>
    

    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 100px;margin-bottom: 50px;padding-bottom: 50px; padding-left: 40px;padding-right: 40px;border: 1px solid #b0b3b7;">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #fff;">
              <div class="lokasi col-lg-10 col-md-10 col-sm-10 col-xs-12" style="padding-top: 20px;padding-bottom: 0px;margin-top: 0px;text-align: left;">
                <h3 style="color: #5780c1;margin-top: 15px; font-weight: bold;"><i class="fa fa-info-circle" aria-hidden="true"></i> Tentang Perusahaan</h3>
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #fff;">
            <div class="lokasi col-md-12 col-sm-12 col-xs-12" style="padding-top: 0px;padding-bottom: 20px;margin-top: 0px;text-align: left;">
                <h4 style="font-family: 'Saira Semi Condensed',sans-serif;line-height: 25px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Fermentum leo vel orci porta non pulvinar. Enim sed faucibus turpis in eu mi bibendum neque egestas. Leo duis ut diam quam. Nibh tellus molestie nunc non blandit massa enim. Cursus euismod quis viverra nibh cras. Massa massa ultricies mi quis hendrerit dolor magna eget. Metus vulputate eu scelerisque felis imperdiet. Elit sed vulputate mi sit. Vitae nunc sed velit dignissim. Pharetra pharetra massa massa ultricies mi quis hendrerit. Pulvinar sapien et ligula ullamcorper malesuada. Phasellus egestas tellus rutrum tellus pellentesque. Sed faucibus turpis in eu mi bibendum neque egestas. Sit amet massa vitae tortor condimentum lacinia quis vel eros. Facilisi etiam dignissim diam quis enim lobortis scelerisque. Tincidunt ornare massa eget egestas purus viverra accumsan in. Eu tincidunt tortor aliquam nulla facilisi cras fermentum odio. Viverra nibh cras pulvinar mattis nunc sed blandit libero. Massa tempor nec feugiat nisl pretium.

                Viverra nam libero justo laoreet sit amet cursus sit. Habitasse platea dictumst quisque sagittis purus. Tortor vitae purus faucibus ornare. Etiam erat velit scelerisque in dictum non consectetur. Et tortor consequat id porta nibh. Felis eget velit aliquet sagittis. Praesent elementum facilisis leo vel fringilla. Est lorem ipsum dolor sit amet consectetur. Mauris in aliquam sem fringilla ut morbi tincidunt augue interdum. Eget egestas purus viverra accumsan. At lectus urna duis convallis convallis tellus id interdum velit.</h4>
              
            </div>
            </div>
          </div>
        </div>
      </div> 
    </div>
    <div class="container-fluid perusa" style="padding-bottom: 50px;padding-top: 100px;">
      <div class="row">
        <div class="container">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami lowpeker " style="margin-top: 0px; text-align: left;overflow: hidden;display: block;margin-bottom: 5px;background-color: #fff; border: 1px solid #eeeeee;box-shadow: 0 1px 2px 0 rgba(0,0,0,0.05);transition: box-shadow .5s, background-color 0.5s;">
              <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style="background-color: #fff;" style="text-align: left;padding-top: 20px;padding-bottom: 10px;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-bottom: 0.25px solid #e8e8e8;">
                  <a href="Profilperusahaan.php" style="text-decoration: none;font-family: 'Saira Semi Condensed', sans-serif;"><h2 style="color: #bf3654;letter-spacing: 0px;">PT.Persero Indonesia</h2></a> 
                  <h5 style="color: #000d21;">Jl.Hassanudin RT 02/ RW 06 , Desa Junrejo , Kecamatan Junrejo.</h5>
                </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="background-color: #fff;text-align: center;padding-top: 10px;padding-bottom: 10px;">
                <img src="http://www.posindonesia.co.id/wp-content/uploads/2016/11/Logo-Pos-Indonesia-2012.jpg" style="width: 100px; height: 100px;">
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #fff;text-align: left;">
              <h4 style="font-weight: bold;padding-left: 15px;">Kota Batu</h4>
              </div>
          </div>
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami lowpeker " style="margin-top: 0px; text-align: left;overflow: hidden;display: block;margin-bottom: 5px;background-color: #fff; border: 1px solid #eeeeee;box-shadow: 0 1px 2px 0 rgba(0,0,0,0.05);transition: box-shadow .5s, background-color 0.5s;">
              <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style="background-color: #fff;" style="text-align: left;padding-top: 20px;padding-bottom: 10px;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-bottom: 0.25px solid #e8e8e8;">
                  <a href="Profilperusahaan.php" style="text-decoration: none;font-family: 'Saira Semi Condensed', sans-serif;"><h2 style="color: #bf3654;letter-spacing: 0px;">PT.Persero Indonesia</h2></a> 
                  <h5 style="color: #000d21;">Jl.Hassanudin RT 02/ RW 06 , Desa Junrejo , Kecamatan Junrejo.</h5>
                </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="background-color: #fff;text-align: center;padding-top: 10px;padding-bottom: 10px;">
                <img src="http://www.posindonesia.co.id/wp-content/uploads/2016/11/Logo-Pos-Indonesia-2012.jpg" style="width: 100px; height: 100px;">
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #fff;text-align: left;">
              <h4 style="font-weight: bold;padding-left: 15px;">Kota Batu</h4>
              </div>
          </div>
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami lowpeker " style="margin-top: 0px; text-align: left;overflow: hidden;display: block;margin-bottom: 5px;background-color: #fff; border: 1px solid #eeeeee;box-shadow: 0 1px 2px 0 rgba(0,0,0,0.05);transition: box-shadow .5s, background-color 0.5s;">
              <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12" style="background-color: #fff;" style="text-align: left;padding-top: 20px;padding-bottom: 10px;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-bottom: 0.25px solid #e8e8e8;">
                  <a href="Profilperusahaan.php" style="text-decoration: none;font-family: 'Saira Semi Condensed', sans-serif;"><h2 style="color: #bf3654;letter-spacing: 0px;">PT.Persero Indonesia</h2></a> 
                  <h5 style="color: #000d21;">Jl.Hassanudin RT 02/ RW 06 , Desa Junrejo , Kecamatan Junrejo.</h5>
                </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" style="background-color: #fff;text-align: center;padding-top: 10px;padding-bottom: 10px;">
                <img src="http://www.posindonesia.co.id/wp-content/uploads/2016/11/Logo-Pos-Indonesia-2012.jpg" style="width: 100px; height: 100px;">
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #fff;text-align: left;">
              <h4 style="font-weight: bold;padding-left: 15px;">Kota Batu</h4>
              </div>
          </div>
          
          <nav aria-label="Page navigation">
            <ul class="pagination">
              <li>
                <a href="#" aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                </a>
              </li>
              <li><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li>
                <a href="#" aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;padding-top: 20px;padding-bottom: 20px;">
          <div class="container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                <h2 style="color: #ccc;font-family: 'Saira Semi Condensed', sans-serif;">Follow Us</h2>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 20px; text-align: center;">
                      <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                      </div>
                      <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;letter-spacing: 10px;">
                        <a href="#" class="sosfoot"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-github" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                      </div>
                      <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                      </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn footeer" style="margin-top: 0px; text-align: center;">
          <div class="container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn footeer" style="margin-top: 0px; text-align: center;padding-bottom: 20px;">
                <h4 style="color: #ccc;"><i class="fa fa-copyright" aria-hidden="true"></i> Copyright. All rights reserved | Designed by Febri Fahturrohman</h4>
              </div>
             
          </div>
        </div>
      </div>
    </div>








<!-- jQuery online menggunakan CDN -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  
<!-- jQuery lokal -->
    <script src="assets/custom/jquery/jquery.min.js"></script>
     <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- Custom JS -->
    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="assets/custom.js"></script>
    <script type="text/javascript">
        var getHeaderHeight = $('.headerContainerWrapper').outerHeight();
        var borderAmount = 2;
        var shadowAmount = 30
        var lastScrollPosition = 0;
        $('.headerContainerWrapper').css('top', '-' + (getHeaderHeight + shadowAmount + borderAmount) + 'px');

        $(window).scroll(function() {
          var currentScrollPosition = $(window).scrollTop();

          if ($(window).scrollTop() > 2 * (getHeaderHeight + shadowAmount + borderAmount) ) {

            $('body').addClass('scrollActive').css('padding-top', getHeaderHeight);
            $('.headerContainerWrapper').css('top', 0);

            if (currentScrollPosition < lastScrollPosition) {
              $('.headerContainerWrapper').css('top', '-' + (getHeaderHeight + shadowAmount + borderAmount) + 'px');
            }
            lastScrollPosition = currentScrollPosition;

          } else {
            $('body').removeClass('scrollActive').css('padding-top', 0);
          }
        });</script>
    <script src="assets/custom/js/wow.min.js"></script>
    <script>
      new WOW().init();
    </script>
    <script src="assets/custom/js/slick.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $('.regular').slick({
            dots: true,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,         
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            dots: true,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            dots: true,
            infinite: true,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true,
            infinite: true,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
          ]
          });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript">
      $(window).load(function() {
          $(".loader").fadeOut("slow");
      });
    </script>
    <script type="text/javascript">
      jQuery(document).ready(function ($) {
        $('#tabs').tab();
      });
    </script>
   </body>
 </html>