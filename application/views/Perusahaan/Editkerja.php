 <html lang="en">
   <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>LOKER</title>
 
     <!-- Bootstrap -->
     <link href="<?php echo base_url() ?>assets/perusahaan/bootstrap/css/bootstrap.min.css" rel="stylesheet">
 
     <!-- HTML5 shim and Respond.js for IE8 support of 
          HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
 
    <!-- Custom CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/perusahaan/custom/css/font-awesome.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/perusahaan/custom/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Berkshire+Swash|Courgette|Quicksand:300|Saira+Semi+Condensed" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/perusahaan/custom/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/perusahaan/custom/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/perusahaan/custom/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/perusahaan/custom/css/slick-theme.css">
    <style type="text/css">
        html, body {
          margin: 0;
          padding: 0;
        }

        * {
          box-sizing: border-box;
        }

        .slider {
            width: 100%;
            margin: 0px auto;
        }

        .slick-slide {
          margin: 0px 20px;
        }

        .slick-slide img {
          width: 100%;
          background-size: cover;
        }

        .slick-prev:before,
        .slick-next:before {
          color: black;
        }


        .slick-slide {
          transition: all ease-in-out .3s;
          opacity: .2;
        }
        
        .slick-active {
          opacity: 1;
        }

        .slick-current {
          opacity: 1;
        }
        .special-img 
        {
          position: relative;
          top: -10px;
          float: left;
          left: -5px;
          width: 40px;
          height: 40px;
          margin-right: 5px;
        }
        .avataropsi{
          margin-top: 10px !important;
          padding-left:10px;
          padding-right: 10px;
        }
    </style>
    <style>
      #contactForm .floating-label-form-group {
        font-size: 16px;
        position: relative;
        margin-bottom: 5px;
        padding-bottom:   0px;
        border-bottom: 1px solid #ddd;
      }

      #contactForm .floating-label-form-group.floating-label-form-group-with-focus {
        position: relative;
      }

      #contactForm .floating-label-form-group:before {
        display: block;
        position: absolute;
        right: 50%;
        bottom: -1px;
        width: 0;
        height: 2px;
        background-color: #bf3654;
        content: "";
        transition: width 0.4s ease-in-out;
      }
      #contactForm .floating-label-form-group:after {
        display: block;
        position: absolute;
        left: 50%;
        bottom: -1px;
        width: 0;
        height: 2px;
        background-color: #bf3654;
        content: "";
        transition: width 0.4s ease-in-out;
      }
      #contactForm .floating-label-form-group.floating-label-form-group-with-focus:before,#contactForm .floating-label-form-group.floating-label-form-group-with-focus:after {
        width: 50%;
      }

      #contactForm .floating-label-form-group input,
      #contactForm .floating-label-form-group textarea {
        z-index: 1;
        position: relative;
        padding-right: 0;
        padding-left: 0;
        border: none;
        border-radius: 0;
        font-size: 16px;
        font-family: "Helvetica", "Arial", sans-serif;
        font-weight: lighter;
        background: none;
        box-shadow: none !important;
        resize: none;
      }

      #contactForm .floating-label-form-group label {
        display: block;
        z-index: 0;
        position: relative;
        top: 0em;
        margin: 0;
        font-size: 14px;
        color: #5a5b59;
        font-family: "Saira Semi Condensed", sans-serif;
        font-weight: lighter;
        vertical-align: bottom;
        opacity: 0;
        -webkit-transition: top 0.3s ease, opacity 0.3s ease;
        -moz-transition: top 0.3s ease, opacity 0.3s ease;
        -ms-transition: top 0.3s ease, opacity 0.3s ease;
        transition: top 0.3s ease, opacity 0.3s ease;
      }
      #contactForm .floating-label-form-group::not(:first-child) {
        padding-left: 14px;
        border-left: 1px solid #eeeeee;
      }

      #contactForm .floating-label-form-group-with-value label {
        top: 5px;
        opacity: 1;
      }

      #contactForm .floating-label-form-group-with-focus label {
        color: #bf3654;
        font-weight: bold;
      }

      #contactForm {
        border-top: 1px solid #ddd;
      }

      #contactForm textarea.form-control {
        height: auto;
      }

      #contactForm .form-control {
        display: block;
        width: 100%;
        color: #000;
      }

      #contactForm input:focus,
      #contactForm input:active,
      #contactForm textarea:focus,
      #contactForm textarea:active {
        outline: none;
      }
      .fileUpload {
          position: relative;
          overflow: hidden;
          margin: 10px;
      }
      .fileUpload input.upload {
          position: absolute;
          top: 0;
          right: 0;
          margin: 0;
          padding: 0;
          font-size: 20px;
          cursor: pointer;
          opacity: 0;
          filter: alpha(opacity=0);
      }
    </style>
    <link href="<?php echo base_url() ?>assets/perusahaan/custom/css/style.css" rel="stylesheet">
 
   </head>
   <body>
    <div class="loader"></div>
    <div class="container-fluid" style="margin: 0px;padding: 0px;">
      <!-- Bagian Header -->
      <div class="kerjaa col-lg-12 col-md-12 col-sm-12 col-xs-12" id="site-header" >
          <header>
            <div class="col-lg-12 col-md-12 col-xs-12 sosmed header" id="site-header" style="height: 40px; max-height: 40px;background-color: rgba(0,0,0,0.26);">
              <div class="col-lg-12 col-xs-12 col-md-12">
                <ul class="sosmed nav navbar-nav navbar-right">
                  <li><a href="#" class="sos"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="sos"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="sos"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#" class="sos"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                </ul>
              </div>
            </div>
            <nav class="navbar navbar-default col-lg-12 headerContainerWrapper" style="background-color: rgba(0,0,0,0.26); padding-top: 10px;padding-bottom: 10px; border: none; border-top: 0.1px solid #848484;  border-radius: 0px;">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" style="color: #fff;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>

                  <a class="navbar-brand" style="color: #fff;font-weight: bold;font-family: 'Saira Semi Condensed', sans-serif;" href="<?php echo site_url('perusahaan/')?>"><span class="glyphicon glyphicon-send" aria-hidden="true"></span> Lowongan Kerja</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo site_url('perusahaan/tentang')?>">Tentang Kami</a></li>
                    <li><a href="<?php echo site_url('perusahaan/kontak')?>">Kontak</a></li>
                    <li><a href="<?php echo site_url('perusahaan/pekerjaan')?>">Pekerjaan <span class="sr-only">(current)</span></a></li>
                    <li><a href="<?php echo site_url('perusahaan/pelamar')?>">Pelamar</a></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle profile-image" data-toggle="dropdown">
                        <img src="<?php echo base_url('assets/logoperusahaan/'.$this->session->userdata('perusahaan_logo')) ?>" class="img-circle special-img">
                         <?php echo " ".strtoupper($this->session->userdata('perusahaan_nama')); ?></a>
                        <ul class="dropdown-menu avataropsi">
                          <li><a href="<?php echo site_url('perusahaan/akun')?>"><i class="fa fa-cog"></i> Pengaturan Akun</a></li>
                          <li class="divider"></li>
                          <li><a href="<?php echo base_url('login/logout'); ?>"><i class="fa fa-sign-out"></i> Keluar</a></li>
                        </ul>
                    </li>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>
          </header>
          <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12 subhead" style="padding-top: 120px; padding-left: 50px;display: inline-grid;padding-bottom: 250px;">
                            <h1 style="color: #fff; font-weight: bold;font-size: 42px; text-shadow: 1px 1px 3px #000;"></h1>
          </div>
    </div>
              
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12" style="margin-top: 30px;">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="panel panel-default single-form loker-form">
                <div class="panel-heading">
                    <h3>
                        <i class="icon icon-dropdown_cari_agen"></i> <span><i class="fa fa-edit" aria-hidden="true"></i>&nbsp&nbsp Edit Pekerjaan</span>
                    </h3>
                </div>
                <?php foreach ($pekerjaan as $datper) {?>
                <form action="<?php echo base_url(). 'perusahaan/update'; ?>" method="post">
                    <div class="panel-body" >
                     
                        <div class="col-md-12">
                            <div class="floating-label-form-group">
                              <label>Nama Pekerjaan </label>
                              <input type="hidden" name="pekerjaan_id" value="<?php echo $datper->pekerjaan_id ?>">
                              <input type="hidden" name="perusahaan_nama" value="<?php echo $this->session->userdata('perusahaan_nama') ?>">
                              <input type="text" class="form-control" placeholder="Nama Pekerjaan" id="nama_pekerjaan" name="nama_pekerjaan" required="required" value="<?php echo $datper->pekerjaan_nama ?>">
                            </div>
                            <div class="floating-label-form-group">
                              <label >Tipe Gajian</label>
                              <select class="form-control" id="tipe_gajian" name="tipe_gajian" style="border:none;box-shadow: none;padding: 0px;">
                                <option hidden style="color: #ccc !important;">Tipe Gajian</option>
                                <option value="Harian">Harian</option>
                                <option value="Mingguan">Mingguan</option>
                                <option value="Bulanan">Bulanan</option>
                              </select>
                            </div>

                            <div class="floating-label-form-group">
                              <label for="comment">Alamat</label>
                             <textarea class="form-control" rows="5" placeholder="Alamat" id="perusahaan_alamat" name="perusahaan_alamat" style="resize: none;">
                               <?php echo $datper->perusahaan_alamat ?>
                             </textarea>
                            </div>
                            <div class="form-group floating-label-form-group is-empty" style="margin-bottom: 10px;">
                                <label class="control-label">Nomor Telepon / Handphone</label>
                                <input class="form-control required" placeholder="Nomor Telepon" type="number" name="perusahaan_nopon" value="<?php echo $datper->perusahaan_nopon ?>">
                            </div>
                            <div class="floating-label-form-group">
                              <label>Syarat Usia</label>
                              <input type="number" class="form-control" placeholder="Syarat Usia" id="umur" name="umur" required="required" value="<?php echo $datper->umur ?>">
                            </div>
                            <div class="floating-label-form-group">
                              <label>Pendidikan</label>
                              <input type="text" class="form-control" placeholder="Pendidikan" id="pendidikan" name="pendidikan" required="required" value="<?php echo $datper->pendidikan ?>">
                            </div>
                            <div class="form-group">
                              <label for="comment">Jenis Kelamin</label>
                              <div class="radio">
                                <label><input type="radio" name="jenis_kelamin" value="Laki">Laki-Laki</label>
                                <label style="margin-left: 5px;"><input type="radio" name="jenis_kelamin" value="Perempuan" >Perempuan</label>
                              </div>
                            </div>
                            <div class="floating-label-form-group">
                              <label for="comment">Tentang Pekerjaan</label>
                              <textarea class="form-control"  rows="5" placeholder="Tentang Pekerjaan" id="tentang_pekerjaan" name="tentang_pekerjaan" style="resize: none;padding-left: 0px;text-align: left;">
                                <?php echo $datper->tentang_pekerjaan ?>
                              </textarea>
                            </div>  
                            <br>
                            <button type="submit" class="btn btn-success">EDIT</button>
                            <a href="<?php echo base_url('Perusahaan/pekerjaan/'); ?>" class="button btn btn-danger">BATAL</a>
                        </div>
                    </div>
                </form>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
     
                
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;padding-top: 20px;padding-bottom: 20px;">
          <div class="container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                <h2 style="color: #ccc;font-family: 'Saira Semi Condensed', sans-serif;">Follow Us</h2>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 20px; text-align: center;">
                      <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                      </div>
                      <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;letter-spacing: 10px;">
                        <a href="#" class="sosfoot"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-github" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href="#" class="sosfoot"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                      </div>
                      <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn foot" style="margin-top: 0px; text-align: center;">
                      </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn footeer" style="margin-top: 0px; text-align: center;">
          <div class="container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timkami wow zoomIn footeer" style="margin-top: 0px; text-align: center;padding-bottom: 20px;">
                <h4 style="color: #ccc;"><i class="fa fa-copyright" aria-hidden="true"></i> Copyright. All rights reserved | Designed by Febri Fahturrohman</h4>
              </div>
             
          </div>
        </div>
      </div>
    </div>








<!-- jQuery online menggunakan CDN -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  
<!-- jQuery lokal -->
    <script src="<?php echo base_url() ?>assets/perusahaan/custom/jquery/jquery.min.js"></script>
     <script src="<?php echo base_url() ?>assets/perusahaan/bootstrap/js/bootstrap.min.js"></script>
    <!-- Custom JS -->
    <script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="<?php echo base_url() ?>assets/perusahaan/custom.js"></script>
    <script type="text/javascript">
        var getHeaderHeight = $('.headerContainerWrapper').outerHeight();
        var borderAmount = 2;
        var shadowAmount = 30
        var lastScrollPosition = 0;
        $('.headerContainerWrapper').css('top', '-' + (getHeaderHeight + shadowAmount + borderAmount) + 'px');

        $(window).scroll(function() {
          var currentScrollPosition = $(window).scrollTop();

          if ($(window).scrollTop() > 2 * (getHeaderHeight + shadowAmount + borderAmount) ) {

            $('body').addClass('scrollActive').css('padding-top', getHeaderHeight);
            $('.headerContainerWrapper').css('top', 0);

            if (currentScrollPosition < lastScrollPosition) {
              $('.headerContainerWrapper').css('top', '-' + (getHeaderHeight + shadowAmount + borderAmount) + 'px');
            }
            lastScrollPosition = currentScrollPosition;

          } else {
            $('body').removeClass('scrollActive').css('padding-top', 0);
          }
        });</script>
    <script src="<?php echo base_url() ?>assets/perusahaan/custom/js/wow.min.js"></script>
    <script>
      new WOW().init();
    </script>
    <script src="<?php echo base_url() ?>assets/perusahaan/custom/js/slick.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $('.regular').slick({
            dots: true,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,         
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            dots: true,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            dots: true,
            infinite: true,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true,
            infinite: true,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
          ]
          });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript">
$(window).load(function() {
    $(".loader").fadeOut("slow");
});
</script>
<script type="text/javascript">
    $(window).load(function() {
        $(".loader").fadeOut("slow");
    });
    </script>
    <script type="text/javascript">
      $(function() {
      $("body").on("input propertychange", ".floating-label-form-group", function(e) {
        $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
      }).on("focus", ".floating-label-form-group", function() {
        $(this).addClass("floating-label-form-group-with-focus");
      }).on("blur", ".floating-label-form-group", function() {
        $(this).removeClass("floating-label-form-group-with-focus");
      });
    });
    </script>
    <script type="text/javascript"></script>
   </body>
 </html>